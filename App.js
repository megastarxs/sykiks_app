import React from 'react'
import {
  Platform,
  View,
  Text,
  TextInput,
  BackHandler,
  AppState
} from 'react-native'
import { AppLoading, Asset, Font, Notifications } from 'expo'
import { Ionicons } from '@expo/vector-icons'
import { Root, StyleProvider } from 'native-base'
import _ from 'lodash'
import { Provider } from 'react-redux'

import StatusBar from './screens/main/components/StatusBar'
import ProfileModal from './screens/shared/ProfileModal'
import store from './redux'

import AppNavigator from './screens'
import { images, navigator } from '@services'

import 'intl'
import 'intl/locale-data/jsonp/en'

import getTheme from './native-base-theme/components'

/* eslint-disable */

const _console = _.clone(console)
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message)
  }
}
console.disableYellowBox = true

/* eslint-enable */

let components = [Text, TextInput]

const customProps = {
  style: {
    fontFamily: 'Lato'
  }
}

for (let i = 0; i < components.length; i++) {
  const TextRender = components[i].prototype.render
  const initialDefaultProps = components[i].prototype.constructor.defaultProps
  components[i].prototype.constructor.defaultProps = {
    ...initialDefaultProps,
    ...customProps
  }
  components[i].prototype.render = function render() {
    let oldProps = this.props
    this.props = { ...this.props, style: [customProps.style, this.props.style] }
    try {
      return TextRender.apply(this, arguments)
    } finally {
      this.props = oldProps
    }
  }
}

var areIntlLocalesSupported = require('intl-locales-supported')

var localesMyAppSupports = []

if (global.Intl) {
  // Determine if the built-in `Intl` has the locale data we need.
  if (!areIntlLocalesSupported(localesMyAppSupports)) {
    // `Intl` exists, but it doesn't have the data we need, so load the
    // polyfill and patch the constructors we need with the polyfill's.
    var IntlPolyfill = require('intl')
    Intl.NumberFormat = IntlPolyfill.NumberFormat
    Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat
  }
} else {
  // No `Intl`, so use and load the polyfill.
  global.Intl = require('intl')
}

export default class App extends React.Component {
  state = {
    notification: {}
  }
  componentDidMount() {
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    )
  }
  componentWillMount() {
    if (Platform.OS === 'android')
      BackHandler.addEventListener('hardwareBackPress', function() {
        return true
      })
  }
  _handleNotification = notification => {
    if (
      AppState.currentState != 'active' &&
      notification.data &&
      notification.data.roomId
    ) {
      navigator.navigate('Inbox', { roomId: `${notification.data.roomId}` })
    }
  }

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      )
    } else {
      return (
        <Provider store={store}>
          <StyleProvider style={getTheme()}>
            <Root>
              <View style={{ flex: 1 }}>
                <AppNavigator />
              </View>
              <StatusBar />
              <ProfileModal />
            </Root>
          </StyleProvider>
        </Provider>
      )
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync(_.values(_.omit(images, 'user-default'))),
      Font.loadAsync(
        // This is the font that we are using for our tab bar
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        {
          ...Ionicons.font,
          Lato: require('./assets/fonts/Lato-Regular.ttf'),
          Lato_bold: require('./assets/fonts/Lato-Bold.ttf'),
          Roboto: require('native-base/Fonts/Roboto.ttf'),
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
          Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf'),
          MaterialCommunityIcons: require('@expo/vector-icons/fonts/MaterialCommunityIcons.ttf'),
          Zocial: require('@expo/vector-icons/fonts/Zocial.ttf'),
          Entypo: require('@expo/vector-icons/fonts/Entypo.ttf')
        }
      )
    ])
  }

  _handleLoadingError = () => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    // console.warn(error)
  }

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  }
}
