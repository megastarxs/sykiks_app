const initState = {}

export default function(state = initState, action) {
  switch (action.type) {
    case 'UPDATE_PROFILE_MODAL_ID':
      return {
        ...state,
        uid: action.uid
      }
    default:
      return state
  }
}

export const updateProfileModalId = resp => {
  return {
    type: 'UPDATE_PROFILE_MODAL_ID',
    uid: resp
  }
}
