const initState = {
  favorite: false,
  categories: {},
  sortBy: 'Top Psychics'
}

export default function(state = initState, action) {
  switch (action.type) {
    case 'UPDATE_PSYCHIC_FILTERS':
      return { ...state, ...action.filters }
    default:
      return state
  }
}

export const updatePsychicFilters = resp => {
  return {
    type: 'UPDATE_PSYCHIC_FILTERS',
    filters: resp
  }
}
