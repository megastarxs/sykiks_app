import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import {
  reactReduxFirebase,
  firebaseReducer,
  getFirebase
} from 'react-redux-firebase'
import thunk from 'redux-thunk'
import { Sykiks } from '@services'
import psychicFilters from './reducers/psychics-filters'
import profileModal from './reducers/profile-modal'
const rrfConfig = {
  userProfile: 'users',
  resetBeforeLogin: true,
  enableLogging: false,
  sessions: null
}

const initialState = {}

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  psychicFilters,
  profileModal
})

const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(
      thunk.withExtraArgument(getFirebase) // Pass getFirebase function as extra argument
    ),
    reactReduxFirebase(Sykiks.firebase, rrfConfig)
  )
)

export default store
