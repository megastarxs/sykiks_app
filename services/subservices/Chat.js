import { now, uid as client } from './Sykiks'
import firebase from 'firebase/app'

let firebaseDB = firebase.database()
let chatRequestRef = firebaseDB.ref(`chat-requests`)
let chatSessionsRef = firebaseDB.ref('chat-sessions')
let settingsRef = firebaseDB.ref('settings')
let chatsRef = firebaseDB.ref('chats')

let Chat = {}

Chat.initClientRequest = psychic => {
  chatRequestRef.push({
    time: now,
    status: 'init',
    client,
    psychic
  })
}

Chat.removeRequest = reqId => {
  chatRequestRef.child(reqId).remove()
}

Chat.denyRequest = reqId => {
  chatRequestRef.child(reqId).update({ status: 'rejected' })
}

Chat.acceptRequest = (reqId, profile, psychic, client) => {
  let sessionKey = chatSessionsRef.push().key
  chatRequestRef.child(reqId).update({ status: 'accepted', sessionKey })
  chatSessionsRef.child(sessionKey).update({
    sessionKey,
    chatRate: profile.chatRate,
    psychic,
    client,
    amount: 0,
    acceptedAt: now
  })

  Chat.setCurrentSessionKey(psychic, sessionKey)
}

Chat.setCurrentSessionKey = (uid, currentSessionKey) => {
  settingsRef.child(uid).update({ currentSessionKey })
}

Chat.endCurrentSession = uid => {
  settingsRef.child(`${uid}/currentSessionKey`).remove()
}

Chat.startSession = sessionKey => {
  Chat.systemMessage({ sessionKey, message: 'Paid session has started' })
  Chat.updateSession(sessionKey, {
    amount: 0,
    sessionStarted: true,
    sessionStartedAt: now
  })
}

Chat.updateSession = (sessionKey, details) => {
  chatSessionsRef.child(sessionKey).update(details)
}

Chat.systemMessage = ({ sessionKey, message }) => {
  let from = 'system'
  chatsRef.child(sessionKey).push({
    from,
    message,
    time: now
  })
}

Chat.sendChat = ({ sessionKey, from, message }) => {
  chatsRef.child(sessionKey).push({
    from,
    message,
    time: now
  })
}

export default Chat
