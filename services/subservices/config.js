var config = {}
config.links = {}
config.links.sykiks = 'https://sykiks.net/privacy-policy'
config.privacyPolicyUrl = 'https://sykiks.net/privacy-policy'
config.aboutusUrl = 'https://sykiks.net/about-us'
config.version = '0.1'
config.termsUrl = 'https://sykiks.net/terms'
config.websiteUrl = 'https://sykiks.net'
config.paymentUrl = 'https://sykiks-mvp.firebaseapp.com/api/pay'

export default config
