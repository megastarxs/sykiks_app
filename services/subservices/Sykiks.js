import firebase from 'firebase'
import _ from 'lodash'
import whoAmI from './whoAmI'
import firebaseConfig from './../../firebase-config'
import { notification } from '@services'

let Sykiks = {}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

Sykiks.firebase = firebase

let firebaseDB = firebase.database()

var now = firebase.database.ServerValue.TIMESTAMP
let uid
Sykiks.now = now

let settingsRef = firebaseDB.ref('settings')
Sykiks.usersRef = firebaseDB.ref('users')
let reviewsRef = firebaseDB.ref('reviews')
let reportRef = firebaseDB.ref('reports')
let chatSessionRef = firebaseDB.ref('chat-sessions')

let userInfoRef = firebaseDB.ref('user-info')
let token

Sykiks.init = async function() {
  token = await whoAmI.getToken()
  Sykiks.token = token

  firebaseDB.ref('.info/connected').on('value', function(snapshot) {
    if (Sykiks.checkMyStatus) Sykiks.checkMyStatus(snapshot.val())
  })
}

function truncate(string) {
  if (string.length > 20) return string.substring(0, 20) + '...'
  else return string
}

Sykiks.truncate = truncate

Sykiks.sendMessage = messageToBeSent => {
  let { profile } = Sykiks
  let { message, roomId, balance } = messageToBeSent

  let clientId = roomId.split('-')[0]
  let psychicId = roomId.split('-')[1]

  if (profile.type == 'client') {
    firebase
      .ref(`users/${psychicId}/msgRate`)
      .once('value')
      .then(s => {
        let msgRate = s.val()
        if (balance < msgRate)
          return notification.showToast(
            "You don't have enough credits to send this message"
          )
        else
          settingsRef
            .child(`${uid}/messages-listing/${psychicId}`)
            .update({
              from: psychicId,
              roomId,
              lastMessage: message,
              fromServer: false,
              time: now,
              msgRate
            })
            .then(() =>
              notification.showToast(
                `Message sent. ${msgRate} deducted from your account `
              )
            )
      })
  } else
    settingsRef
      .child(`${uid}/messages-listing/${clientId}`)
      .update({
        from: clientId,
        roomId,
        lastMessage: message,
        fromServer: false,
        time: now,
        msgRate: profile.msgRate
      })
      .then(() => notification.showToast(`Message sent.`))
}

Sykiks.markAsRead = id => {
  settingsRef.child(`${uid}/unread/${id}`).remove()
}

Sykiks.clientRoomId = to => {
  return uid + '-' + to
}

Sykiks.init()

Sykiks.handleAppStateChange = nextAppState => {
  if (uid) userInfoRef.child(uid).update({ online: nextAppState == 'active' })
}

Sykiks.setProfile = async (puid, profile) => {
  // let online = true

  Sykiks.uid = puid
  uid = puid
  Sykiks.profile = profile

  let { token } = Sykiks
  var myConnectionsRef = firebase.database().ref(`users/${uid}/online/mobile`)
  var connectedRef = firebase.database().ref('.info/connected')
  connectedRef.on('value', function(snap) {
    if (snap.val()) {
      myConnectionsRef.set(true)
      firebase.auth().onAuthStateChanged(function(user) {
        if (!user) myConnectionsRef.remove()
      })
      myConnectionsRef.onDisconnect().remove()
    }
  })

  if (!uid) return
  if (token) firebase.ref(`settings/${uid}/tokens/expo`).set(token)

  // userInfoRef.child(uid).update({ online, token })
}

Sykiks.giveReview = (uid, obj, sessionKey) => {
  obj.postedOn = now
  reviewsRef.child(uid).push(obj)
  chatSessionRef.child(sessionKey).update({ reviewd: true })
  notification.showToast('Review submitted')
}

Sykiks.totalRating = profile => {
  let ratings = (profile.rating_up || 0) + (profile.rating_down || 0)
  if (ratings) return ratings + ' ratings'
  return 'No ratings'
}

Sykiks.percentageRating = profile => {
  if (profile.rating_up || profile.rating_down) {
    let n = profile.rating_up || 0
    let d = n + (profile.rating_down || 0)
    if (d) return Math.round((n / d) * 100) + '%'
  }
  return 'NA'
}

Sykiks.formatDateandTime = ts => {
  if (ts) {
    ts = new Date(ts)
    if (_.isDate(ts))
      return Intl.DateTimeFormat(undefined, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit'
      }).format(ts)
  }
  return 'NA'
}

Sykiks.formatDate = ts => {
  if (ts) {
    ts = new Date(ts)
    if (_.isDate(ts))
      return Intl.DateTimeFormat(undefined, {
        year: 'numeric',
        month: 'short',
        day: 'numeric'
      }).format(ts)
  }
  return 'NA'
}

Sykiks.formatTime = ts => {
  if (ts) {
    ts = new Date(ts)
    if (_.isDate(ts))
      return Intl.DateTimeFormat(undefined, {
        hour: '2-digit',
        minute: '2-digit'
      }).format(ts)
  }
  return 'NA'
}

Sykiks.formatCurrrecy = number => {
  return Intl.NumberFormat('en', { style: 'currency', currency: 'USD' }).format(
    number
  )
}

Sykiks.toArray = obj => {
  return _.keys(obj)
    .reverse()
    .map(k => {
      obj[k].key = k
      return obj[k]
    })
}

Sykiks.fixFloat = num => {
  if (!num) return 0
  return Math.round(num * 100) / 100
}

Sykiks.updatePaypal = paypalId => {
  settingsRef.child(`${uid}/accountInfo`).update({ paypalId })
}

Sykiks.reportUser = obj => {
  obj.time = now
  obj.reportedById = uid
  reportRef.push(obj)
}

Sykiks.blockUser = blockedById => {
  settingsRef.child(`${uid}/blocked/${blockedById}`).set(true)
}

export default Sykiks
export { uid, now }
