import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import _ from 'lodash'
import Sykiks from './Sykiks'
import spinnerWhileLoading from './spinnerWhileLoading'
import { updateProfileModalId } from './../../redux/reducers/profile-modal'
import { compose, onlyUpdateForKeys } from 'recompose'

let easyFirebase = paths => {
  if (paths == 'auth')
    return compose(
      firebaseConnect(),
      connect(({ firebase: { profile, auth } }) => ({
        profile,
        auth
      }))
    )
  let updateKeys = []

  let loadingKeys = []
  if (_.isArray(paths)) {
    let omitKeys = ['unread', 'profile']
    loadingKeys = paths.map(d => d.replace('@', ''))
    updateKeys = [...loadingKeys]
    if (updateKeys.includes('psychics')) updateKeys.push('psychicFilters')
    loadingKeys = loadingKeys.filter(d => !omitKeys.includes(d))
  }

  var mapDispatchToProps = { updateProfileModalId }

  return compose(
    firebaseConnect((props, firebase) => {
      let newPaths = []
      let authUid = firebase.firebase._.authUid
      let params = props.navigation && props.navigation.state.params
      paths.forEach(d => {
        if (d == 'psychics')
          newPaths.push({
            path: 'users',
            queryParams: ['orderByChild=type', 'equalTo=psychic'],
            storeAs: 'psychics'
          })
        else if (d == 'inboxMsgs')
          newPaths.push({
            path: 'messages/' + params.roomId,
            storeAs: 'inboxMsgs_' + params.roomId
          })
        else if (d == 'chatSessions')
          newPaths.push({
            path: 'chat-sessions/' + params.sessionKey,
            storeAs: 'chatSessions_' + params.sessionKey
          })
        else if (d == 'chats')
          newPaths.push({
            path: 'chats/' + params.sessionKey,
            storeAs: 'chats_' + params.sessionKey
          })
        else if (d == 'reviews') {
          newPaths.push({
            path: `reviews/` + params.k,
            storeAs: 'reviews_' + params.k
          })
        } else if (d == 'inboxList')
          newPaths.push({
            path: `settings/${authUid}/messages-listing`,
            storeAs: 'inboxList'
          })
        else if (d == 'reviewsSelf')
          newPaths.push({
            path: `reviews/` + authUid,
            storeAs: 'reviewsSelf'
          })
        else if (d == 'chat-sessions-client')
          newPaths.push({
            path: 'chat-sessions',
            queryParams: ['orderByChild=client', 'equalTo=' + authUid],
            storeAs: 'chat-sessions-client'
          })
        else if (d == 'chat-sessions-psychic')
          newPaths.push({
            path: 'chat-sessions',
            queryParams: ['orderByChild=psychic', 'equalTo=' + authUid],
            storeAs: 'chat-sessions-psychic'
          })
        else if (!d.path && d.includes('@')) {
          newPaths.push({
            storeAs: d.replace('@', ``),
            path: d.replace('@', `settings/${authUid}/`)
          })
        } else if (d.path) newPaths.push(d)
      })
      return newPaths
    }),
    connect(
      ({ firebase, psychicFilters, profileModal }, props) => {
        let obj = {}
        paths.forEach(d => {
          if (d == 'psychics') obj['psychicFilters'] = psychicFilters
          if (d == 'profileModal') obj['profileModal'] = profileModal

          if (d.includes('@')) {
            let storeAs = d.replace('@', '')
            var ref = d.replace('@', `settings/${firebase.auth.uid}/`)
            obj[storeAs] = firebase.data[storeAs]
            if (Sykiks.firebase) obj[storeAs + 'Ref'] = Sykiks.firebase.ref(ref)
          } else if (
            _.includes(
              [
                'inboxList',
                'chat-sessions-client',
                'chat-sessions-psychic',
                'reviewsSelf',
                'psychics'
              ],
              d
            )
          ) {
            obj[d] = firebase.data[d]
          } else if (
            _.includes(['inboxMsgs', 'chatSessions', 'chats', 'reviews'], d)
          ) {
            let params = props.navigation && props.navigation.state.params
            let key = params.roomId || params.sessionKey
            if (d == 'reviews') key = params.k
            obj[d] = firebase.data[`${d}_${key}`]
          } else if (d == 'profile') {
            obj.profile = firebase.profile
            obj.profile.uid = firebase.auth.uid
          }
        })

        return obj
      },
      mapDispatchToProps
    ),
    onlyUpdateForKeys(updateKeys),
    spinnerWhileLoading(loadingKeys)
  )
}

export default easyFirebase
