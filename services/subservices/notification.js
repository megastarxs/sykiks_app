import { Toast } from 'native-base'

let notification = {}

// // const soundObject = new Audio.Sound();

// export function playHelloSound(){
//     // soundObject.playAsync();
// }
// // Expo.Notifications.addListener()

notification.showToast = function(msg, opts = {}) {
  Toast.show({
    text: msg,
    position: opts.position || 'top',
    // buttonText: 'Okay',
    ...opts,
    duration: 7000
  })
}

export default notification
