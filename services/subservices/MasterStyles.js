import colors from './colors'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  whiteBackground: {
    backgroundColor: colors.white
  },
  purpleBackground: {
    backgroundColor: colors.purple
  },
  center: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch'
  },
  btn: {
    marginTop: 10,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
    // textAlign:'center',
    // fontWeight:'bold'
  }
})
