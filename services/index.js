import notification from './subservices/notification'
import images from './subservices/images'
import renderIf from './subservices/renderIf'
import whoAmI from './subservices/whoAmI'
import t from './subservices/translations'
import Sykiks from './subservices/Sykiks'
import Chat from './subservices/Chat'
import config from './subservices/config'
import colors from './subservices/colors'
import easyFirebase from './subservices/easyFirebase'
import MasterStyles from './subservices/MasterStyles'
import navigator from './subservices/navigator'
import spinnerWhileLoading from './subservices/spinnerWhileLoading'

export {
  notification,
  images,
  renderIf,
  whoAmI,
  Sykiks,
  Chat,
  t,
  config,
  colors,
  easyFirebase,
  MasterStyles,
  navigator,
  spinnerWhileLoading
}
