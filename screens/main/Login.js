import React, { Component } from 'react'
import { Icon, Input, List, ListItem, InputGroup, Button } from 'native-base'
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Text
} from 'react-native'
import { Entypo } from '@expo/vector-icons'
import { notification, colors, images, Sykiks } from '@services'
import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect()
export default class Login extends Component {
  state = {}
  user = {
    email: '',
    password: ''
  }

  async submit() {
    if (this.user.email == '')
      return notification.showToast('Please enter your Email')

    if (this.user.password == '')
      return notification.showToast('Please enter a password')

    try {
      await this.props.firebase.login(this.user)
      Sykiks.handleAppStateChange('active')
    } catch (error) {
      notification.showToast('Username or password is not correct')
    }
  }

  render() {
    let { navigate } = this.props.navigation
    return (
      <ImageBackground
        source={images['login-background']}
        style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.logoContainer}>
            <Image source={images['login-logo']} style={styles.logo} />
          </View>
          <List style={styles.form}>
            <ListItem style={styles.listitem}>
              <InputGroup>
                <Entypo name="email" style={styles.entypoFont} />
                <Input
                  placeholder="Email"
                  placeholderTextColor="#CFCFCF"
                  style={styles.listInput}
                  onChangeText={text => (this.user.email = text)}
                />
              </InputGroup>
            </ListItem>
            <ListItem style={styles.listitem}>
              <InputGroup>
                <Icon name="md-lock" style={styles.listIcon} />
                <Input
                  placeholder="Password"
                  placeholderTextColor="#CFCFCF"
                  style={styles.listInput}
                  secureTextEntry
                  onChangeText={text => (this.user.password = text)}
                />
              </InputGroup>
            </ListItem>
          </List>
          <View style={styles.logoContainer}>
            <View style={styles.buttonView}>
              <Button
                style={[styles.btn, styles.submitBtn]}
                onPress={this.submit.bind(this)}>
                <Text style={{ color: colors.white }}>SIGN IN</Text>
              </Button>
            </View>
            <View>
              <Text style={{ color: '#fff' }}>Don't have an account?</Text>
            </View>
            <View>
              <Button
                transparent
                onPress={() => navigate('Registration', { type: 'client' })}
                style={styles.registerAsClient}>
                <Text style={styles.registerAsClientText}>
                  Register as Client
                </Text>
              </Button>
            </View>
            <View>
              <Button transparent style={styles.orBtn}>
                <Text style={styles.orText}>-OR-</Text>
              </Button>
            </View>
            <View>
              <Button
                transparent
                onPress={() => navigate('Registration', { type: 'psychic' })}
                style={styles.registerAsPsychic}>
                <Text style={styles.registerAsPsychicText}>
                  Register as Psychic
                </Text>
              </Button>
            </View>
            <View>
              <Button transparent onPress={() => navigate('Forgotpassword')}>
                <Text style={{ textAlign: 'center', color: '#fff' }}>
                  Forgot Password ?
                </Text>
              </Button>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 20
  },
  contentContainer: {
    flex: 1,
    alignSelf: 'stretch',
    width: null,
    padding: 20
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 20,
    backgroundColor: colors.white
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 140,
    height: 170,
    margin: 15
  },
  form: {
    width: '80%',
    borderWidth: 2,
    borderRadius: 6,
    borderColor: '#f4f4f5',
    marginLeft: '10%',
    marginRight: '10%'
    // paddingBottom:-20,
  },
  submitBtn: {
    backgroundColor: colors.purple_icon
  },
  btn: {
    marginTop: 10,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    // textAlign:'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
    // fontWeight:'bold'
  },
  about: {
    fontSize: 12,
    color: '#8394c0',
    fontWeight: '300',
    fontStyle: 'italic'
  },
  nextButton: {
    width: 80
  },
  bar: {
    marginTop: 100,
    backgroundColor: '#00FF00'
  },
  listitem: {
    marginLeft: 0,
    paddingLeft: 10,
    backgroundColor: colors.white
  },
  listIcon: {
    color: '#CFCFCF',
    fontSize: 24
  },
  heading: {
    fontSize: 18,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 10,
    color: '#696969',
    textAlign: 'center'
  },
  entypoFont: {
    color: '#CFCFCF',
    fontSize: 20,
    paddingLeft: 5,
    paddingRight: 5
  },
  registerBtn: {
    backgroundColor: '#7cb031',
    alignSelf: 'stretch',
    alignItems: 'center'
  },
  buttonView: {
    width: '70%',
    marginLeft: '10%',
    marginRight: '10%',
    alignItems: 'center'
  },
  registerAsClient: {
    paddingBottom: 0,
    height: 30,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white
  },
  registerAsPsychic: {
    paddingTop: 0,
    paddingBottom: 0,
    height: 30,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white
  },
  registerAsPsychicText: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16
  },
  registerAsClientText: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16
  },
  orText: {
    textAlign: 'center',
    color: '#fff',
    paddingTop: 0,
    paddingBottom: 0
  },
  orBtn: {
    paddingTop: 0,
    paddingBottom: 0,
    height: 30
  }
})
