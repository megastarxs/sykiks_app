export default `Thank you for taking time to read through our terms and conditions. Please note that “on the site” “our” “us” “we” “our platform” “our website” are all terms used to describe and refer to sykiks.com

1. You understand that as a client/member it is your responsibility to verify any information provided by any advisor and you do so at your own risk
2. As a member you agree to being solely responsible for all of the content that you submit from your account and are responsible for the full accuracy, and validity of information that you provide during your interactions with advisors on the site.
3. You agree that you shall not provide any advisor with personal contact information such as your address or telephone number, or any other information which relates to a similar nature.
4. As a member you must not directly provide the advisor with any payment information such as credit card details or PayPal addresses. All payments must go through the legitimate payment channels/gateways that sykiks.com have provided.
5. You understand that you must be fully authorized as the account holder of your credit card/debit card and or PayPal account when purchasing any of our advisors services.
6. You must ensure that all payment details that you provide upon purchasing services from advisors on sykiks.com are accurate and up to date.
7. You agree that you will be subject to your chosen payment gateways terms and conditions, as well as the terms and conditions of sykiks.com.
8. sykiks.com informs you that requesting charge backs and seeking refunds is a breach of our terms and conditions and can be classed as fraudulent activity.
9. You understand that all payment transactions are non refundable and sykiks.com will not be liable for unsatisfactory services.
10. You understand that this agreement applies to all users and members of the Site including all third party independent advisors.
11. sykiks.com may make changes to this agreement on occasion without notifying you directly.
12. You understand that your continued use of the Services on sykiks.com will automatically mean that you have accepted, agreed and kept up to date with any changes made to the terms and conditions/agreements and privacy policy on our site.
13. If you do not accept updated versions of our services, terms, conditions and policies then sykiks.com shall not bear any responsibility or liability for your decision to continue using the services provided on its site.
14. You understand that your right to access the Site or Services will be revoked where this agreement or use of the Site or the Services is prohibited and, if that is the case, you agree not to use or access the site or services in any way.
15. You agree and are aware of the fact that sykiks.com may end your membership and restrict you from entering the Site and its Services with or without prior notice with no reason or explanation.`
