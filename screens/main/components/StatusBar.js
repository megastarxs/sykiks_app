import React from 'react'
import { Text, NetInfo, StyleSheet, AppState } from 'react-native'
import { Sykiks, colors } from '@services'
import { Badge } from 'native-base'
import { Fade } from '@shared'

export default class Element extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: true,
      message: 'Connecting...',
      color: colors.info
    }
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', Sykiks.handleAppStateChange)
  }
  componentDidMount() {
    AppState.addEventListener('change', Sykiks.handleAppStateChange)

    var self = this
    Sykiks.checkMyStatus = function(online) {
      if (!online) {
        self.setState({
          visible: true,
          message: 'Connecting...',
          color: colors.info
        })
      } else {
        self.setState({
          visible: true,
          message: 'Connected',
          color: colors.info
        })
        setTimeout(function() {
          self.setState({
            visible: false
          })
        }, 500)
      }
    }

    NetInfo.isConnected.addEventListener('connectionChange', function(
      isConnected
    ) {
      if (!isConnected)
        self.setState({
          visible: true,
          message: 'Error connecting to the servers...',
          color: colors.error
        })
      else
        self.setState({
          visible: false
        })
    })
  }

  render() {
    let { visible, message } = this.state
    return (
      <Fade isVisible={visible} style={styles.wrapper}>
        <Badge info style={styles.badgeStyle}>
          <Text style={styles.badgeText}>{message} </Text>
        </Badge>
      </Fade>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 20,
    left: '40%'
  },
  badgeStyle: {
    padding: 10,
    margin: 2,
    backgroundColor: colors.purple
  },
  badgeText: {
    color: '#fff',
    fontWeight: 'bold'
  }
})
