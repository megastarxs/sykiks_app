import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  KeyboardAvoidingView
} from 'react-native'
import {
  Content,
  Icon,
  Input,
  List,
  ListItem,
  InputGroup,
  Thumbnail,
  Button,
  Spinner
} from 'native-base'
import { ImagePicker } from 'expo'
import { Entypo } from '@expo/vector-icons'
import { colors, images, notification, Sykiks } from '@services'
import Popover, { PopoverTouchable } from 'react-native-modal-popover'
import { firebaseConnect } from 'react-redux-firebase'
import { CheckboxGroup } from '@shared'

@firebaseConnect()
export default class Registration extends Component {
  profile = {
    screenName: '',
    aboutme: ''
  }

  user = {
    email: '',
    password: '',
    confirmPassword: ''
  }

  constructor(props) {
    super(props)

    let { type } = this.props.navigation.state.params

    this.state = {
      image: images['user-default'],
      isLoading: false,
      categoriesLabel: 'Specialites'
    }

    if (type == 'psychic') {
      let { state, profile } = this
      profile.categories = 'Specialites'
      let categories = [
        'Psychic Readings',
        'Tarot',
        'Spiritual counselling',
        'Astrology',
        'Dream Interpretation',
        'Love and Relationships',
        'Numerology'
      ]
      state.categories = categories.map((d, i) => ({ label: d }))
    }
  }

  setCategories(index) {
    let { categories } = this.state
    var item = categories[index]
    item.selected = !item.selected
    let categoriesLabel = categories
      .filter(d => d.selected)
      .map(d => d.label)
      .join(', ')
    if (categoriesLabel == '') categoriesLabel = 'Specialites'

    this.profile.categories = categoriesLabel
    this.setState({ categories, categoriesLabel })
  }

  async submit() {
    let { type } = this.props.navigation.state.params
    let { profile, user } = this

    if (profile.screenName == '')
      return notification.showToast('Please enter screenName')

    if (user.email == '')
      return notification.showToast('Please enter your email id')

    if (user.password == '')
      return notification.showToast('Please enter a password')

    if (user.confirmPassword == '')
      return notification.showToast('Please enter Confirm Password')

    if (user.password != user.confirmPassword)
      return notification.showToast(
        'Password and Confirm Password do not match'
      )

    if (type == 'psychic' && profile.categories == 'Specialites') {
      return notification.showToast('Please select atleast one category')
    }

    this.setState({ isLoading: true })

    try {
      profile.type = type
      profile.createdAt = Sykiks.now
      if (type == 'psychics') {
        profile.status = 'Available'
        profile.rating_down = 0
        profile.rating_up = 0
        profile.chatRate = 0
        profile.msgRate = 0
      }

      let { firebase } = this.props
      let user2 = await firebase.createUser(user, profile)
      let uid = firebase.auth().currentUser.uid
      if (user2) {
        firebase
          .auth()
          .currentUser.sendEmailVerification()
          .then(function() {
            notification.showToast('Email sent. Please check your email')
          })
        if (this.image) {
          let obj = {}
          obj[uid] = this.image
          firebase.ref(`images`).update(obj)
        }
      }
    } catch (err) {
      notification.showToast(err.message)
      this.setState({ isLoading: false })
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    })

    if (result.cancelled) return

    result.base64 = 'data:image/jpeg;base64,' + result.base64
    var image = result.base64
    this.image = image
    this.setState({ image })
  }

  render() {
    let { navigate } = this.props.navigation
    let { isLoading, categoriesLabel } = this.state
    let { type } = this.props.navigation.state.params

    return (
      <Content>
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.logoContainer}>
            <TouchableOpacity onPress={this._pickImage.bind(this)}>
              <Thumbnail
                source={{ uri: this.state.image }}
                style={styles.logo}
              />
            </TouchableOpacity>
          </View>
          <List style={styles.form}>
            <ListItem style={styles.listitem}>
              <InputGroup>
                <Icon name="ios-person" style={styles.listIcon} />
                <Input
                  placeholder="Screen name"
                  placeholderTextColor="#CFCFCF"
                  style={styles.listInput}
                  onChangeText={text => (this.profile.screenName = text)}
                />
              </InputGroup>
            </ListItem>
            <ListItem style={styles.listitem}>
              <InputGroup>
                <Entypo name="email" style={styles.entypoFont} />
                <Input
                  placeholder="Email"
                  placeholderTextColor="#CFCFCF"
                  style={styles.listInput}
                  onChangeText={text => (this.user.email = text)}
                />
              </InputGroup>
            </ListItem>
            <ListItem style={styles.listitem}>
              <InputGroup>
                <Icon name="md-lock" style={styles.listIcon} />
                <Input
                  placeholder="Password"
                  placeholderTextColor="#CFCFCF"
                  style={styles.listInput}
                  secureTextEntry
                  onChangeText={text => (this.user.password = text)}
                />
              </InputGroup>
            </ListItem>
            <ListItem style={styles.listitem}>
              <InputGroup>
                <Icon name="md-lock" style={styles.listIcon} />
                <Input
                  placeholder="Confirm Password"
                  placeholderTextColor="#CFCFCF"
                  style={styles.listInput}
                  secureTextEntry
                  onChangeText={text => (this.user.confirmPassword = text)}
                />
              </InputGroup>
            </ListItem>
            {type == 'psychic' && (
              <ListItem style={styles.listitem}>
                <InputGroup>
                  <Entypo name="open-book" style={styles.entypoFont} />
                  <Input
                    placeholder="About Me"
                    placeholderTextColor="#CFCFCF"
                    style={styles.listInput}
                    onChangeText={text => (this.profile.aboutme = text)}
                  />
                </InputGroup>
              </ListItem>
            )}
            {type == 'psychic' && (
              <ListItem style={styles.listitem}>
                <PopoverTouchable>
                  <TouchableOpacity>
                    <Text style={styles.speciality}>
                      <Icon name="ios-list" style={styles.listIcon} />
                      {'   '} {categoriesLabel}
                    </Text>
                  </TouchableOpacity>
                  <Popover arrowStyle={{ display: 'none' }}>
                    <CheckboxGroup
                      callback={this.setCategories.bind(this)}
                      checkboxes={this.state.categories}
                    />
                  </Popover>
                </PopoverTouchable>
              </ListItem>
            )}
          </List>
          <View style={styles.logoContainer}>
            <View style={styles.buttonView}>
              {isLoading ? (
                <Button style={[styles.btn, styles.submitBtn]}>
                  <Spinner color="#fff" />
                </Button>
              ) : (
                <Button
                  style={[styles.btn, styles.submitBtn]}
                  onPress={this.submit.bind(this)}>
                  <Text style={{ color: colors.white }}>Register</Text>
                </Button>
              )}
            </View>
            <View>
              <Text style={{ color: '#000' }}>Already have an account?</Text>
            </View>
            <View style={styles.buttonView}>
              <Button
                iconLeft
                style={[styles.btn, styles.registerBtn]}
                onPress={() => navigate('Login')}>
                <Text
                  uppercase
                  style={{ textAlign: 'center', color: colors.white }}>
                  Login
                </Text>
              </Button>
            </View>
          </View>
        </KeyboardAvoidingView>
      </Content>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 20
  },
  contentContainer: {
    flex: 1,
    alignSelf: 'stretch',
    width: null,
    padding: 20
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 20,
    backgroundColor: colors.white
  },
  logoContainer: {
    alignItems: 'center',
    marginTop: 20
  },
  logo: {
    width: 140,
    height: 140,
    margin: 15
  },
  form: {
    width: '80%',
    borderWidth: 2,
    borderRadius: 6,
    borderColor: '#f4f4f5',
    marginLeft: '10%',
    marginRight: '10%'
    // paddingBottom:-20,
  },
  submitBtn: {
    backgroundColor: colors.purple_icon,
    marginTop: 0
  },
  btn: {
    marginTop: 10,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    // textAlign:'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
    // fontWeight:'bold'
  },
  about: {
    fontSize: 12,
    color: '#8394c0',
    fontWeight: '300',
    fontStyle: 'italic'
  },
  nextButton: {
    width: 80
  },
  bar: {
    marginTop: 100,
    backgroundColor: '#00FF00'
  },
  listitem: {
    marginLeft: 0,
    paddingLeft: 10,
    backgroundColor: colors.white
  },
  listIcon: {
    color: '#CFCFCF',
    fontSize: 24
  },
  heading: {
    fontSize: 18,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 10,
    color: '#696969',
    textAlign: 'center'
  },
  entypoFont: {
    color: '#CFCFCF',
    fontSize: 20,
    paddingLeft: 5,
    paddingRight: 5
  },
  registerBtn: {
    backgroundColor: '#7cb031',
    alignSelf: 'stretch',
    alignItems: 'center'
  },
  buttonView: {
    width: '70%',
    marginLeft: '10%',
    marginRight: '10%',
    alignItems: 'center'
  },
  speciality: {
    color: '#CFCFCF',
    fontSize: 17,
    paddingLeft: 5,
    paddingRight: 5
  }
})
