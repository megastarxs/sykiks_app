import React, { Component } from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import TosText from './components/TosText'
import {
  Container,
  Header,
  Content,
  Button,
  Card,
  CardItem,
  Body
} from 'native-base'
import { images, colors } from '@services'

export default class Tos extends Component {
  render() {
    const { callback } = this.props
    return (
      <Container>
        <Header style={styles.header}>
          <Body>
            <Image
              source={images['header']}
              resizeMode="cover"
              style={styles.headerImage}
            />
          </Body>
        </Header>
        <Content>
          <Card>
            <CardItem>
              <Body>
                <View>
                  <Text style={styles.tosTitle}>Terms & Conditions</Text>
                  <Text style={styles.tosText}>{TosText}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>
          <View>
            <Button block style={styles.buttonStyle} onPress={callback}>
              <Text style={styles.buttonText}> I AGREE </Text>
            </Button>
          </View>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    marginTop: 15
  },
  backArrowIcon: {
    color: '#7E7E7E'
  },
  headerImage: {
    width: 140,
    height: 40,
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  tosTitle: {
    fontSize: 20,
    color: '#4E4E4E',
    marginBottom: 10
  },
  tosText: {
    color: '#4E4E4E'
  },
  buttonStyle: {
    backgroundColor: colors.white
  },
  buttonText: {
    color: colors.purple
  },
  purpleBack: {
    backgroundColor: colors.purple
  }
})
