import React, { Component } from 'react'
import { Text, StyleSheet, TextInput, KeyboardAvoidingView } from 'react-native'
import { Button, View, Spinner } from 'native-base'
import { colors, MasterStyles, notification } from '@services'
import { firebaseConnect } from 'react-redux-firebase'

@firebaseConnect()
export default class ForgotPassowrd extends Component {
  state = {
    email: '',
    isLoading: false
  }

  forgotPassowrd() {
    this.setState({ isLoading: true })
    if (this.state.email == '') {
      this.setState({ isLoading: false })
      return notification.showToast('Please enter your Email')
    }
    var self = this
    this.props.firebase
      .auth()
      .sendPasswordResetEmail(this.state.email)
      .then(function() {
        self.props.navigation.navigate('Login')
        self.setState({ isLoading: false })
        return notification.showToast('Email sent on respective email Id')
      })
      .catch(function(error) {
        self.setState({ isLoading: false })
        return notification.showToast(error.message)
      })
  }

  render() {
    return (
      <View
        style={[
          MasterStyles.whiteBackground,
          { justifyContent: 'center', flex: 1, alignSelf: 'stretch' }
        ]}>
        <KeyboardAvoidingView behavior="padding">
          <View style={styles.bar}>
            <Text style={styles.headerTitle}>Forgot Password</Text>
          </View>
          <View style={styles.bar}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.balanceTitle}>Email</Text>
              <TextInput
                style={styles.values}
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={text => {
                  this.setState({ email: text })
                }}
              />
            </View>
          </View>

          <View style={styles.flexRow}>
            {this.state.isLoading ? (
              <Button style={styles.btn}>
                <Spinner color="#fff" />
              </Button>
            ) : (
              <Button style={styles.btn}>
                <Text
                  style={{ color: colors.white }}
                  onPress={this.forgotPassowrd.bind(this)}>
                  FORGOT PASSWORD
                </Text>
              </Button>
            )}
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    // borderBottomWidth: 0.5,
    // borderBottomColor: colors.lightestgrey,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  balanceTitle: {
    color: colors.GreyTitle,
    marginHorizontal: 10,
    marginVertical: 7,
    // textAlign: 'right',
    fontSize: 12
    // width:'20%'
  },
  values: {
    fontSize: 14,
    height: 50,
    margin: 10,
    marginTop: -5,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    backgroundColor: '#FBFBFB',
    width: '75%',
    padding: 10
  },
  headerTitle: {
    marginVertical: 7,
    // marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 20
    // textAlign:'center'
  },
  btn: {
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    // alignSelf: 'stretch',
    justifyContent: 'center',
    width: '60%',

    marginLeft: '20%',
    backgroundColor: colors.purple_icon,
    height: 40
  },
  flexRow: {
    alignItems: 'center'
  }
})
