import React, { Component } from 'react'
import { StyleSheet, Text, FlatList } from 'react-native'
import { Content, Button, View } from 'native-base'
import { colors, easyFirebase, Sykiks, MasterStyles } from '@services'
import { FontAwesome } from '@expo/vector-icons'
import {
  LeftComponent,
  RightComponent,
  SystemMessage,
  ProfileModalHandler,
  ThumbnailByUid,
  ScreenNameByUid
} from '@shared'
import _ from 'lodash'

@easyFirebase(['chats', 'profile'])
export default class ChatSession extends Component {
  renderItem({ item }) {
    if (item.from == 'system') return <SystemMessage {...item} />
    if (this.props.profile.uid == item.from) return <RightComponent {...item} />
    else return <LeftComponent {...item} />
  }
  render() {
    let history = this.props.navigation.state.params
    let { chats, profile } = this.props
    let { navigate } = this.props.navigation
    let partnerUid = profile.type == 'client' ? history.psychic : history.client
    let chat = []
    _.each(chats, (v, k) => {
      v.key = k
      chat.push(v)
    })
    return (
      <Content style={[MasterStyles.whiteBackground, styles.container]}>
        <View style={styles.mainContainer}>
          <View style={styles.imageContainer}>
            <ProfileModalHandler uid={partnerUid}>
              <ThumbnailByUid uid={partnerUid} style={styles.chatImage} />
            </ProfileModalHandler>
          </View>
          <View style={styles.textComponent}>
            <ProfileModalHandler uid={partnerUid}>
              <Text style={styles.name}>
                {' '}
                <ScreenNameByUid
                  uid={
                    profile.type == 'client' ? history.psychic : history.client
                  }
                />{' '}
              </Text>
            </ProfileModalHandler>
            <View style={styles.flexRow}>
              <View style={[styles.flexRow, styles.padding15]}>
                <Text style={styles.title}>Duration </Text>
                <Text style={styles.value}> {history.sessionTime} </Text>
              </View>
              <View style={styles.flexRow}>
                <Text style={styles.title}>Amount </Text>
                <Text style={styles.value}>
                  {' '}
                  {Sykiks.formatCurrrecy(history.amount)}{' '}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.chatTopContainer}>
          <View style={styles.chatTopTextComponent}>
            <Text style={styles.chatTopText}>
              <FontAwesome
                name="calendar-o"
                size={20}
                style={{ color: colors.lightestgrey }}
              />
              <Text style={styles.calenderDate}>
                {'   '}
                {Sykiks.formatDate(history.acceptedAt)}
              </Text>
              <Text style={styles.calenderTime}>
                {'   '}
                {Sykiks.formatTime(history.acceptedAt)}
              </Text>
            </Text>
          </View>
        </View>
        <FlatList
          data={chat}
          keyExtractor={({ key }) => key}
          renderItem={this.renderItem.bind(this)}
          inverted
        />
        {profile.type == 'client' &&
          !history.reviewed && (
            <View style={{ alignSelf: 'center', marginTop: 30 }}>
              <Button
                style={styles.btn}
                onPress={() =>
                  navigate('ReviewForm2', {
                    partnerUid,
                    sessionKey: history.sessionKey
                  })
                }>
                <Text style={{ color: colors.white }}>Leave a Review</Text>
              </Button>
            </View>
          )}
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainContainer: {
    flexDirection: 'row',
    marginTop: 5,
    padding: 15,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: colors.lightestgrey
  },
  chatImage: {
    width: 50,
    height: 53
  },
  textComponent: {
    alignSelf: 'flex-start',
    marginLeft: 10,
    flexDirection: 'column'
  },
  name: {
    fontWeight: 'bold',
    color: colors.mildGrey,
    fontSize: 17,
    paddingTop: 5,
    paddingBottom: 3
  },
  title: {
    color: colors.mildGrey,
    fontSize: 14
  },
  value: {
    color: colors.mildGrey,
    fontWeight: 'bold'
  },
  chatTopContainer: {
    marginTop: 5,
    marginLeft: 10
  },
  chatTopTextComponent: {
    borderRadius: 10,
    alignSelf: 'center',
    marginRight: 10
  },
  chatTopText: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    color: colors.chatTextGrey,
    flexDirection: 'row'
  },
  imageContainer: {
    borderRadius: 50,
    overflow: 'hidden'
    // paddingRight:15
  },
  flexRow: {
    flexDirection: 'row'
  },
  padding15: {
    paddingRight: 15
  },
  calenderDate: {
    fontSize: 11,
    fontWeight: 'bold',
    paddingLeft: 10,
    paddingRight: 10
  },
  calenderTime: {
    fontSize: 11
  },
  btn: {
    backgroundColor: colors.purple_icon,
    marginTop: -25,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center'
    // textAlign:'center',
    // alignSelf: 'flex-end',
    // fontWeight:'bold'
  }
})
