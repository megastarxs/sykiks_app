import { StackNavigator } from 'react-navigation'
import { headerProps } from '@shared'
import List from './List'
import ReviewForm from './../../client/Psychics/ReviewForm'
import ChatSession from './ChatSession'
import { easyFirebase } from '@services'

export default type => {
  return StackNavigator(
    {
      SessionHistory: {
        screen: easyFirebase(['chat-sessions-' + type, 'profile'])(List)
      },
      ChatSession: {
        screen: ChatSession,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        })
      },
      ReviewForm2: {
        screen: ReviewForm,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        })
      }
    },
    {
      ...headerProps,
      initialRouteName: 'SessionHistory'
    }
  )
}
