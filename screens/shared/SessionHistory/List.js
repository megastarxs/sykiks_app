import React, { Component } from 'react'
import { StyleSheet, Text, FlatList } from 'react-native'
import { Content, ListItem, View } from 'native-base'
import { colors, Sykiks } from '@services'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import _ from 'lodash'

import { isEmpty } from 'react-redux-firebase'
import { DefaultText, ScreenNameByUid } from '@shared'
export default class SessionHistoryList extends Component {
  TransactionCard(navigate, type) {
    return ({ item }) => (
      <ListItem
        onPress={() => {
          navigate('ChatSession', item)
        }}>
        <View style={styles.bar}>
          <View style={[{ width: '50%' }, styles.barItem]}>
            <Text style={styles.barTop}>
              {Sykiks.formatDate(item.acceptedAt)}{' '}
              {Sykiks.formatTime(item.acceptedAt)}{' '}
            </Text>
            <Text style={styles.barBottom}>
              <ScreenNameByUid
                uid={type == 'client' ? item.psychic : item.client}
              />
            </Text>
          </View>
          <View style={[{ width: '30%' }, styles.barItem]}>
            <Text style={styles.barTop}>
              {' '}
              {item.sessionTime ? item.sessionTime : 'Session did not start'}
            </Text>
          </View>
          <View style={[{ width: '20%' }, styles.barItem]}>
            {type == 'client' && (
              <Text style={styles.barTop4Negative}>
                - {Sykiks.formatCurrrecy(item.amount)}
              </Text>
            )}
            {type == 'psychic' && (
              <Text style={styles.barTop4Positive}>
                + {Sykiks.formatCurrrecy(item.amount)}
              </Text>
            )}
          </View>
        </View>
      </ListItem>
    )
  }
  render() {
    let { navigate } = this.props.navigation
    let { profile } = this.props

    let chatSessions = this.props['chat-sessions-' + profile.type]

    if (isEmpty(chatSessions)) return <DefaultText text="No chat session yet" />

    return (
      <Content>
        <View style={{ backgroundColor: colors.white, flex: 1 }}>
          <Text
            style={{
              fontSize: 26,
              paddingLeft: 20,
              paddingTop: 15,
              borderBottomWidth: 0.5,
              borderColor: '#c9c9c9',
              borderTopWidth: 0.5,
              paddingBottom: 15
            }}>
            <MaterialCommunityIcons
              name="message-text"
              style={styles.messageIcon}
            />{' '}
            Session History
          </Text>
          <FlatList
            data={_.values(chatSessions)}
            keyExtractor={({ sessionKey }) => sessionKey}
            renderItem={this.TransactionCard(navigate, profile.type)}
          />
        </View>
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    marginTop: 25
  },
  headerImage: {
    width: 140,
    height: 40
  },
  messageIcon: {
    fontSize: 24,
    paddingLeft: 5,
    paddingRight: 15,
    fontWeight: 'bold'
  },
  clockIcon: {
    fontSize: 16,
    paddingRight: 3
  },
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row'
  },
  barTop: {
    fontSize: 11
  },
  barItem: {
    padding: 5
    // alignItems: 'center',
  },
  barBottom: {
    color: colors.purpleText,
    fontSize: 13
  },
  barTop4Positive: {
    fontSize: 11,
    color: colors.green
  },
  barTop4Negative: {
    fontSize: 11,
    color: 'red'
  }
})
