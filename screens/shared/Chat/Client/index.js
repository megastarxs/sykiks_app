import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import _ from 'lodash'
import { ListItem, Button } from 'native-base'
import { ScreenNameByUid, ImageByUid } from '@shared'
import { colors, MasterStyles } from '@services'
import enhancer from './enhancer'
import styles from '../styles'

let ClientChatRequest = ({ chatRequests, removeRequest, message }) => {
  let open = _.isEmpty(chatRequests)
  if (open) return null
  let requestId = Object.keys(chatRequests)[0]
  let request = chatRequests[requestId]

  return (
    <View style={[styles.content]}>
      <ListItem avatar style={[MasterStyles.whiteBackground, styles.listItem]}>
        {request.status == 'init' && (
          <View>
            <Text
              style={[
                {
                  textAlign: 'center',
                  fontStyle: 'italic',
                  fontSize: 24,
                  paddingTop: '20%'
                }
              ]}>
              {` Contacting `}
              <ScreenNameByUid uid={request.psychic} />
            </Text>
            <ImageByUid
              uid={request.psychic}
              resizeMode="cover"
              style={{
                width: 80,
                height: 80,
                alignSelf: 'center',
                marginTop: 10,
                marginBottom: 10
              }}
            />
            <Text
              style={[
                {
                  fontSize: 20,
                  fontStyle: 'italic',
                  paddingTop: '3%',
                  textAlign: 'center'
                }
              ]}>
              {`Please wait while we connect you`}
            </Text>
            <ActivityIndicator
              size="large"
              color={colors.purple}
              style={[{ marginTop: 50, height: 80 }]}
            />
          </View>
        )}
        {request.status == 'rejected' && (
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: '60%'
            }}>
            <Text
              style={[
                {
                  fontStyle: 'italic',
                  fontSize: 24,
                  textAlign: 'center',
                  paddingTop: '15%'
                }
              ]}>
              {' '}
              <ScreenNameByUid uid={request.psychic} />
              {' is Unavailable. '}
            </Text>
            <ImageByUid
              uid={request.psychic}
              resizeMode="cover"
              style={{
                width: 80,
                height: 80,
                alignSelf: 'center',
                marginTop: 10,
                marginBottom: 10
              }}
            />
            <Text
              style={[
                {
                  fontSize: 20,
                  fontStyle: 'italic',
                  paddingTop: '5%',
                  textAlign: 'center',
                  marginBottom: 10
                }
              ]}>
              {' please leave him a message or try again later '}
            </Text>
          </View>
        )}
        <View style={{ marginBottom: 10 }}>
          {request.status == 'rejected' && (
            <Button
              uppercase
              style={[
                MasterStyles.btn,
                MasterStyles.purpleBackground,
                { backgroundColor: colors.light_purple_button }
              ]}
              onPress={() => message(request.psychic, requestId)}>
              <Text style={{ textAlign: 'center', color: colors.purple_icon }}>
                Leave Message
              </Text>
            </Button>
          )}
          <Button
            uppercase
            style={[MasterStyles.btn, MasterStyles.purpleBackground]}
            onPress={() => removeRequest(requestId)}>
            <Text style={{ textAlign: 'center', color: '#fff' }}>
              Cancel request
            </Text>
          </Button>
        </View>
      </ListItem>
    </View>
  )
}
export default enhancer(ClientChatRequest)
