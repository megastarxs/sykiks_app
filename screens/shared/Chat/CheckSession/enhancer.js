import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isEmpty } from 'react-redux-firebase'
import { navigator } from '@services'
import { lifecycle } from 'recompose'
export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: `settings/${firebase.firebase._.authUid}/currentSessionKey`,
      storeAs: `currentSessionKey`
    }
  ]),
  connect(({ firebase: { data } }) => ({
    currentSessionKey: data.currentSessionKey,
    hasCurrentSession: !isEmpty(data.currentSessionKey)
  })),
  lifecycle({
    componentWillUpdate(props) {
      let currentSessionKey = props.currentSessionKey
      let hasCurrentSession = props.hasCurrentSession

      if (hasCurrentSession) {
        navigator.navigate('Chat', { sessionKey: currentSessionKey })
      }
    }
  })
)
