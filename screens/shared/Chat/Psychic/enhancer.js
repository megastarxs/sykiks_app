import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { withHandlers } from 'recompose'
import { Chat } from '@services'

export default compose(
  firebaseConnect((props, firebase) => [
    {
      path: 'chat-requests',
      queryParams: [
        'orderByChild=psychic',
        `equalTo=${firebase.firebase._.authUid}`
      ],
      storeAs: `chatRequestsPsychic`
    },
    {
      path: `settings/${firebase.firebase._.authUid}/blocked`,
      storeAs: `blocked`
    }
  ]),
  connect(({ firebase: { data, profile, auth } }) => ({
    chatRequests: data.chatRequestsPsychic,
    blocked: data.blocked,
    profile,
    auth
  })),
  withHandlers({
    denyRequest: ({ firebase }) => reqId => {
      Chat.denyRequest(reqId)
    },
    acceptRequest: ({ firebase, profile, auth }) => (reqId, client) => {
      Chat.acceptRequest(reqId, profile, auth.uid, client)
    }
  })
)
