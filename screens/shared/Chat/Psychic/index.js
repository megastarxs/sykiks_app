import React from 'react'
import enhancer from './enhancer'
import { View, Text } from 'react-native'
import _ from 'lodash'
import { ScreenNameByUid, ImageByUid } from '@shared'
import { ListItem, Button } from 'native-base'
import { MasterStyles } from '@services'
import styles from '../styles'

let ClientChatRequest = ({
  chatRequests,
  denyRequest,
  acceptRequest,
  blocked
}) => {
  let open = _.isEmpty(chatRequests)
  if (open) return null
  let requestId = Object.keys(chatRequests)[0]
  let request = chatRequests[requestId]
  if (blocked && blocked[request.client]) return null
  open = request.status != 'init'
  return (
    <View style={styles.content}>
      <ListItem avatar style={[MasterStyles.whiteBackground, styles.listItem]}>
        <View>
          <ImageByUid
            uid={request.client}
            resizeMode="cover"
            style={{
              width: 80,
              height: 80,
              alignSelf: 'center',
              marginBottom: 10
            }}
          />
          <Text style={styles.textItem}>
            <ScreenNameByUid uid={request.client} />
            {' wants to chat with you. '}
          </Text>
        </View>
        <View style={styles.btnWrapper}>
          <Button
            style={[MasterStyles.btn, styles.purpleButton]}
            onPress={() => acceptRequest(requestId, request.client)}>
            <Text style={styles.whiteBtnText}>Accept</Text>
          </Button>
          <Button
            style={[MasterStyles.btn, styles.button]}
            onPress={() => denyRequest(requestId)}>
            <Text style={styles.purpleBtnText}>Deny</Text>
          </Button>
        </View>
      </ListItem>
    </View>
  )
}
export default enhancer(ClientChatRequest)
