import { colors } from '@services'
import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
  content: {
    padding: 15,
    top: '5%',
    position: 'absolute',
    marginLeft: '5%',
    marginRight: '5%',
    width: '90%'
  },
  listItem: {
    borderRadius: 5,
    borderColor: '#000',
    borderWidth: 1,
    padding: 15,
    flex: 1,
    flexDirection: 'column',
    marginLeft: 0
  },
  btnWrapper: {
    flex: 1,
    flexDirection: 'row'
  },
  purpleBtnText: {
    fontSize: 11,
    paddingLeft: 2,
    paddingRight: 2,
    color: colors.purple_icon
  },
  whiteBtnText: {
    fontSize: 11,
    paddingLeft: 2,
    paddingRight: 2,
    color: colors.white
  },
  textItem: {
    textAlign: 'center'
  },
  button: {
    backgroundColor: colors.light_purple_button,
    justifyContent: 'center',
    width: '40%',
    marginRight: '4%',
    padding: 5,
    height: 25,
    marginBottom: 5
  },
  purpleButton: {
    backgroundColor: colors.purple,
    justifyContent: 'center',
    width: '40%',
    marginRight: '2%',
    padding: 5,
    height: 25,
    marginBottom: 5
  }
})

export default styles
