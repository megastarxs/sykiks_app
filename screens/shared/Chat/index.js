import React, { Fragment } from 'react'
import Client from './Client'
import Psychic from './Psychic'
import CheckSession from './CheckSession'
import enhance from './enhancer'
let ChatRequest = ({ profile, loggedIn }) => (
  <Fragment>
    {profile.type == 'client' && <Client />}
    {profile.type == 'psychic' && <Psychic />}
    {loggedIn && <CheckSession />}
  </Fragment>
)

export default enhance(ChatRequest)
