import React from 'react'
import { View, Image } from 'react-native'
import { images } from '@services'

export default ({ children }) => (
  <View
    style={{ alignItems: 'center', justifyContent: 'center', height: '99%' }}>
    <Image
      source={images['mobile-trans']}
      style={{ width: 150, height: 150, margin: 15 }}
    />
    <View>{children}</View>
  </View>
)
