import React from 'react'
import enchancer from './enhancer'
import AlertContainer from './AlertContainer'
import ChatRequest from '../Chat'
import { Text, StyleSheet, View } from 'react-native'
import { Button, Container } from 'native-base'
import { colors } from '@services'
export const Alerts = ({
  profile,
  auth,
  loggedIn,
  children,
  verifyEmail,
  reloadAuth
}) => {
  if (!auth.emailVerified)
    return (
      <AlertContainer>
        <Text style={{ textAlign: 'center', fontSize: 23 }}>WELCOME!</Text>
        <Text style={styles.topLine}>
          You must verify your email address to continue
        </Text>
        <View
          style={{
            borderBottomWidth: 0.5,
            borderColor: '#ccc',
            margin: 10,
            paddingBottom: 15,
            alignItems: 'center'
          }}>
          <Text style={{ fontSize: 15, margin: 5 }}>
            Not Received Verification Email?
          </Text>
          <Button transparent onPress={verifyEmail} style={styles.buttonStyle}>
            <Text style={styles.linkStyle}>Resend Email</Text>
          </Button>
        </View>
        <View style={{ alignItems: 'center', margin: 10 }}>
          <Text style={{ fontSize: 15, margin: 5 }}>Already Verified ?</Text>
          <Button transparent onPress={reloadAuth} style={styles.buttonStyle}>
            <Text style={styles.linkStyle}>Continue To Sykiks</Text>
          </Button>
        </View>
      </AlertContainer>
    )

  if (profile.banned)
    return (
      <AlertContainer>
        <Text style={styles.textLine}> You have been banned </Text>
      </AlertContainer>
    )

  if (profile.status == 'pending')
    return (
      <AlertContainer>
        <Text style={styles.textLine1}>
          {' '}
          Your psychic request is being reviewed.{' '}
        </Text>
        <Text style={styles.textLine}>
          {' '}
          A representative from Sykiks will contact you soon.
        </Text>
      </AlertContainer>
    )

  return (
    <Container>
      {children}
      <ChatRequest />
    </Container>
  )
}
const styles = StyleSheet.create({
  topLine: {
    fontSize: 18,
    textAlign: 'center',
    paddingTop: '5%',
    lineHeight: 25,
    marginLeft: 10,
    marginRight: 10
  },
  buttonStyle: {
    backgroundColor: colors.purple_icon,
    padding: 10,
    marginLeft: 'auto',
    marginRight: 'auto',
    margin: 5
  },
  linkStyle: {
    fontSize: 18,
    color: '#fff',
    fontStyle: 'italic',
    lineHeight: 35
  },
  bottomLine: {
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 35
  },
  textLine: {
    fontSize: 24,
    textAlign: 'center',
    paddingTop: '10%',
    lineHeight: 30
  },
  textLine1: {
    fontSize: 24,
    textAlign: 'center',
    paddingTop: '10%',
    lineHeight: 30
  }
})
export default enchancer(Alerts)
