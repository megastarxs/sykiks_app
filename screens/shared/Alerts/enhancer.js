import { compose } from 'redux'
import { connect } from 'react-redux'
import { withProps, withHandlers } from 'recompose'
import { withFirebase, isEmpty, isLoaded } from 'react-redux-firebase'
import { spinnerWhileLoading, notification } from '@services'

export default compose(
  withFirebase,
  connect(({ firebase: { profile, auth } }) => ({ profile, auth })),
  spinnerWhileLoading(['profile']),
  withHandlers({
    verifyEmail: ({ firebase }) => () => {
      firebase
        .auth()
        .currentUser.sendEmailVerification()
        .then(function() {
          notification.showToast('Email sent. Please check your email')
        })
    },
    reloadAuth: ({ firebase, auth }) => () => {
      notification.showToast(
        'If your email is verified, you would be allowed to continue'
      )
      firebase.reloadAuth()
    }
  }),
  withProps(({ auth, profile }) => ({
    loggedIn: isLoaded(auth) && !isEmpty(auth)
  }))
)
