// import _ from 'lodash'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { withStateHandlers, lifecycle, withHandlers } from 'recompose'
import { spinnerWhileLoading, Chat, notification } from '@services'
import { Platform, BackHandler, Alert } from 'react-native'

let autoStartSessionIn = 3 * 60
// let autoStartSessionIn = 30

export default compose(
  // UserIsAuthenticated,
  firebaseConnect(
    (
      {
        navigation: {
          state: {
            params: { sessionKey }
          }
        }
      },
      firebase
    ) => [
      {
        path: `chats/${sessionKey}`,
        storeAs: `chats_${sessionKey}`
      },
      {
        path: `settings/${firebase.firebase._.authUid}/accountInfo/balance`,
        storeAs: 'balance'
      },
      { path: `.info/serverTimeOffset`, storeAs: `serverTimeOffset` },
      {
        path: `chat-sessions/${sessionKey}`,
        storeAs: `chat-session_${sessionKey}`
      }
    ]
  ),
  connect(
    (
      { firebase: { data, profile, auth } },
      {
        navigation: {
          state: {
            params: { sessionKey }
          }
        }
      }
    ) => ({
      chat: data[`chats_${sessionKey}`],
      session: data[`chat-session_${sessionKey}`],
      balance: data.balance,
      serverTimeOffset: data.serverTimeOffset,
      profile: profile,
      from: auth.uid,
      me: auth.uid,
      sessionKey: sessionKey
    })
  ),
  lifecycle({
    componentDidMount() {
      if (Platform.OS === 'android') {
        BackHandler.addEventListener('hardwareBackPress', () => true)
      }
    }
    // componentWillUpdate(nextProps) {
    //   let current = _.last(_.keys(this.props.chat))
    //   let next = _.last(_.keys(nextProps.chat))

    //   if (current !== next) {
    //     setTimeout(() => {
    //       if (scrollRef) scrollRef.scrollIntoView({ behavior: 'smooth' })
    //     }, 500)
    //   }
    // }
  }),
  withHandlers({
    startTyping: ({ sessionKey, profile }) => () => {
      let obj = {}
      obj[profile.type] = true
      Chat.updateSession(sessionKey, { typing: obj })
    },
    stopTyping: ({ sessionKey, profile }) => () => {
      let obj = {}
      obj[profile.type] = false
      Chat.updateSession(sessionKey, { typing: obj })
    },
    perHalfMinute: ({ sessionKey, session, profile, balance, me }) => (
      seconds,
      sessionTime
    ) => {
      if (profile.type == 'client') {
        if (balance < session.chatRate / 2) {
          notification.showToast(
            "You don't have enough credits to continue this chat session.\n Consider adding credits to continue the session"
          )

          Chat.systemMessage({ sessionKey, message: 'Session has ended' })
          Chat.updateSession(sessionKey, {
            sessionEnded: true,
            sessionTime
          })
          return
        }
        seconds = seconds + 30
        let amount = (session.chatRate * seconds) / 60 // eslint-disable-line
        Chat.updateSession(sessionKey, { amount })
      }
    },
    // registerScrollRef: () => ref => {
    //   scrollRef = ref
    // },
    startSession: ({ sessionKey, session, profile }) => () => {
      if (profile.type == 'psychic')
        if (
          !session ||
          (session && !session.sessionStarted && !session.sessionEnded)
        ) {
          Chat.startSession(sessionKey, session.chatRate / 2)
        }
    }
  }),
  withStateHandlers(
    { sessionTime: '', autoStartSessionIn, message: '' },
    {
      setSessionTime: () => sessionTime => ({
        sessionTime
      }),
      sendChat: ({ message }, { sessionKey, from }) => () => {
        if (message != '') Chat.sendChat({ sessionKey, message, from })
        return { message: '' }
      },

      changeText: () => message => ({ message }),
      endSession: (
        { sessionTime },
        { sessionKey, session, profile, me, navigation: { navigate }, firebase }
      ) => () => {
        Alert.alert(
          '',
          'Are you sure you want to end the session?',
          [
            {
              text: 'Cancel',
              onPress: () => {},
              style: 'cancel'
            },
            {
              text: 'OK',
              onPress: () => {
                if (!session.sessionEnded) {
                  Chat.systemMessage({
                    sessionKey,
                    message: 'Session has ended'
                  })
                  Chat.updateSession(sessionKey, {
                    sessionEnded: true,
                    sessionTime
                  })
                }
                Chat.endCurrentSession(me, profile.type == 'psychic')
                if (profile.type == 'psychic') {
                  firebase.updateProfile({ status: 'Available' })
                  navigate('ProfileBlock')
                }

                if (profile.type == 'client')
                  navigate('ReviewForm', {
                    partnerUid: session.psychic,
                    sessionKey
                  })
              }
            }
          ],
          { cancelable: false }
        )
      }
    }
  ),
  spinnerWhileLoading(['chat', 'session'])
)
