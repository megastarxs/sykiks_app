import React, { Component } from 'react'
import moment from 'moment'
import { Text, View, StyleSheet } from 'react-native'

class StopWatch extends Component {
  state = {
    displayTime: '00:00:00'
  }
  unmount = false

  componentDidMount() {
    let { startTime, start } = this.props
    if (start) {
      this.start(moment(new Date(startTime)))
    }
  }

  componentWillReceiveProps(newProps) {
    let { start, startTime } = newProps
    if (start) {
      this.start(moment(new Date(startTime)))
    } else this.stop()
  }

  stop() {
    clearInterval(this.interval)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
    this.unmount = true
  }

  start(startTime) {
    let {
      callback,
      perHalfMinute,
      serverTimeOffset,
      countdown,
      countdownCallback
    } = this.props
    this.interval = this.interval
      ? this.interval
      : setInterval(() => {
          if (this.unmount) return
          let a = startTime
          let seconds
          var b = moment(new Date())
          if (countdown)
            seconds = Math.round(
              a.diff(b, 'seconds') + countdown - serverTimeOffset / 1000
            )
          else
            seconds = Math.round(
              -1 * a.diff(b, 'seconds') + serverTimeOffset / 1000
            )
          let displayTime = moment()
            .hour(0)
            .minute(0)
            .second(seconds)
            .format('HH:mm:ss')
          if (seconds == 0 && countdownCallback) countdownCallback()
          if (seconds % 30 == 0 && perHalfMinute)
            perHalfMinute(seconds, displayTime)
          this.setState({ displayTime })
          if (callback) callback(displayTime)
        }, 1000)
  }

  render() {
    let { displayTime } = this.state

    return (
      <View ref="stopwatch" style={styles.container}>
        <Text style={styles.text}>{displayTime}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#874EA9',
    padding: 5,
    borderRadius: 5,
    width: 85
  },
  text: {
    fontSize: 15,
    color: '#FFF',
    marginLeft: 7
  }
})

export default StopWatch
