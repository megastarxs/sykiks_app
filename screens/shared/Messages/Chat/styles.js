import { StyleSheet } from 'react-native'
import { colors } from '@services'
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  list: {
    height: '70%',
    marginTop: 5
  },
  footer: {
    // flex: 1,
    flexDirection: 'row',
    height: 60,
    // paddingTop: 15,
    // paddingBottom: 15,
    borderTopWidth: 0.5,
    borderTopColor: '#B6B6B6'
    // marginTop:10
  },
  input: {
    paddingHorizontal: '5%',
    paddingVertical: '3%',
    fontSize: 16,
    width: '80%',
    color: '#000'
  },
  sendButton: {
    // paddingTop: '5%',
    // paddingHorizontal: 10,
    // right: 10,
    // position: 'absolute'
    width: '20%',
    paddingHorizontal: '3%'
  },
  sendButtonText: {
    fontWeight: 'bold',
    color: colors.purple_icon
  },
  btn: {
    height: 30,
    marginLeft: 10
  },
  btnText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 14
  },
  typing: {
    color: colors.lightTextgrey,
    alignSelf: 'flex-start',
    paddingLeft: 10,
    marginBottom: 15,
    marginTop: 10
  },
  endBtn: {
    backgroundColor: '#EB6161'
  },
  playPauseBtn: {
    backgroundColor: '#874EA9'
  },
  sessionEndMessageContainer: {
    backgroundColor: '#F1F1F1',
    alignItems: 'center',
    padding: 15
  },
  sessionEndMessge: {
    color: colors.mildGrey
  },
  amountDeducted: {
    color: colors.purple_icon,
    fontWeight: 'bold'
  },
  balance: {
    fontWeight: 'bold'
  }
})
export default styles
