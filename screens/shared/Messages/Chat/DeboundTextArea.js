import React from 'react'
import _ from 'lodash'
import { TextInput, StyleSheet } from 'react-native'
export default class DebounceTextArea extends React.Component {
  componentDidMount() {
    let { startTyping, stopTyping } = this.props

    this.startTyping = _.debounce(
      function() {
        startTyping()
      },
      500,
      { leading: true, trailing: false }
    )

    this.stopTyping = _.debounce(function() {
      stopTyping()
    }, 500)
  }
  textChange(text) {
    if (text != '') {
      this.props.onChange(text)
      this.startTyping()
      this.stopTyping()
    }
  }

  render() {
    let { onChange, disable, value } = this.props
    return (
      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        placeholder="Type your message ..."
        onChangeText={onChange}
        disabled={disable}
        value={value}
      />
    )
  }
}
const styles = StyleSheet.create({
  input: {
    paddingHorizontal: '5%',
    paddingVertical: '3%',
    fontSize: 16,
    width: '80%',
    color: '#000'
  }
})
