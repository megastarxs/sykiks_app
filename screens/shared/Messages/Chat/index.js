import React, { Fragment } from 'react'
import enhance from './enhancer'
import {
  ScreenNameByUid,
  RightComponent,
  LeftComponent,
  SystemMessage
} from '@shared'
import DeboundTextArea from './DeboundTextArea'
import { Text, View, FlatList, KeyboardAvoidingView } from 'react-native'
import styles from './styles'
import { Header, Title, Left, Right, Button } from 'native-base'
import { colors, Sykiks } from '@services'
import Timer from './Timer'
import { MaterialIcons } from '@expo/vector-icons'

export const Session = ({
  chat,
  message,
  me,
  session,
  profile,
  changeText,
  startSession,
  setSessionTime,
  endSession,
  startTyping,
  stopTyping,
  balance,
  registerTextRef,
  registerScrollRef,
  serverTimeOffset,
  perHalfMinute,
  autoStartSessionIn,
  sendChat
}) => {
  if (!session) return null

  let chats
  if (chat) chats = Sykiks.toArray(chat)

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior="padding"
      keyboardVerticalOffset={120}>
      <Header style={{ backgroundColor: colors.purple_icon }}>
        <Left>
          <Title style={{ padding: 5, color: '#fff' }}>
            {profile.type == 'client' ? (
              <ScreenNameByUid uid={session.psychic} />
            ) : (
              <ScreenNameByUid uid={session.client} />
            )}
          </Title>
        </Left>

        <Right>
          {session.sessionStarted && (
            <Timer
              start={session.sessionStarted && !session.sessionEnded}
              startTime={session.sessionStartedAt}
              callback={setSessionTime}
              perHalfMinute={perHalfMinute}
              serverTimeOffset={serverTimeOffset}
            />
          )}
          {!session.sessionStarted && (
            <Timer
              start={!session.sessionStarted}
              startTime={session.acceptedAt}
              serverTimeOffset={serverTimeOffset}
              countdown={autoStartSessionIn}
              countdownCallback={startSession}
            />
          )}

          {profile.type == 'psychic' &&
            !session.sessionStarted && (
              <Button
                style={[styles.btn, styles.playPauseBtn]}
                onPress={startSession}>
                <Text style={styles.btnText}>
                  <MaterialIcons
                    name="play-arrow"
                    size={15}
                    style={{ color: colors.white }}
                  />{' '}
                  Start
                </Text>
              </Button>
            )}
          <Button onPress={endSession} style={[styles.endBtn, styles.btn]}>
            <Text style={styles.btnText}> X </Text>
          </Button>
        </Right>
      </Header>
      <View style={{ height: '62%' }}>
        {chats && (
          <FlatList
            data={chats}
            keyExtractor={({ key }) => key}
            renderItem={({ item }) => {
              if (item.from == 'system') return <SystemMessage {...item} />
              if (me == item.from) return <RightComponent {...item} />
              else return <LeftComponent {...item} />
            }}
            style={styles.list}
            inverted
          />
        )}
        {session.sessionEnded && (
          <View style={{ alignItems: 'center' }}>
            <Button
              style={{
                backgroundColor: 'red',
                padding: 15,
                alignSelf: 'center'
              }}
              onPress={endSession}>
              <Text style={{ color: '#FFF' }}>Close Session</Text>
            </Button>
          </View>
        )}
      </View>

      <View style={{ height: 40 }}>
        {profile.type == 'client' ? (
          <Fragment>
            {session.typing &&
              session.typing.psychic && (
                <Text style={styles.typing}>
                  <ScreenNameByUid uid={session.psychic} /> typing...
                </Text>
              )}
          </Fragment>
        ) : (
          <Fragment>
            {session.typing &&
              session.typing.client && (
                <Text style={styles.typing}>
                  <ScreenNameByUid uid={session.client} /> typing...
                </Text>
              )}
          </Fragment>
        )}
      </View>
      {profile.type == 'client' && (
        <View style={styles.sessionEndMessageContainer}>
          <Text style={styles.sessionEndMessge}>
            {' '}
            Currently used{' '}
            <Text style={styles.amountDeducted}>
              {' '}
              {Sykiks.formatCurrrecy(session.amount)}{' '}
            </Text>{' '}
            of your{' '}
            <Text style={styles.balance}>
              {' '}
              {Sykiks.formatCurrrecy(session.amount + balance)}{' '}
            </Text>{' '}
            balance
          </Text>
        </View>
      )}
      {profile.type == 'psychic' && (
        <View style={styles.sessionEndMessageContainer}>
          <Text style={styles.sessionEndMessge}>
            {' '}
            Earned from current session{' '}
            <Text style={styles.amountDeducted}>
              {' '}
              {Sykiks.formatCurrrecy(session.amount)}{' '}
            </Text>{' '}
          </Text>
        </View>
      )}

      <View style={styles.footer}>
        <DeboundTextArea
          onChange={changeText}
          startTyping={startTyping}
          stopTyping={stopTyping}
          value={message}
          disable={session.sessionEnded}
        />
        <Button transparent light style={styles.sendButton} onPress={sendChat}>
          <Text style={styles.sendButtonText}>SEND</Text>
        </Button>
      </View>
    </KeyboardAvoidingView>
  )
}

export default enhance(Session)
