import { StackNavigator } from 'react-navigation'
import { headerProps } from '@shared'
import List from './List'
import Inbox from './Inbox'

export default StackNavigator(
  {
    MessagesList: {
      screen: List,
      navigationOptions: ({ navigation }) => ({})
    },
    Inbox: {
      screen: Inbox,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: false
      })
    }
  },
  {
    ...headerProps,
    initialRouteName: 'MessagesList'
  }
)
