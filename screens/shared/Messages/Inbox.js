import React from 'react'
import _ from 'lodash'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  FlatList,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native'

import { Body, Left, ListItem, Button } from 'native-base'
import { colors, easyFirebase, Sykiks } from '@services'

import {
  DefaultText,
  ProfileModalHandler,
  ThumbnailByUid,
  ScreenNameByUid
} from '@shared'
import { isEmpty } from 'react-redux-firebase'

let amountClass = {
  client: 'redAmount',
  psychic: 'greenAmount'
}

@easyFirebase(['inboxMsgs', 'profile', '@unread', '@accountInfo'])
export default class Inbox extends React.Component {
  state = {
    message: ''
  }
  constructor(props) {
    super(props)
    let toSend = this.props.navigation.state.params

    this.messageToBeSent = {
      roomId: toSend.roomId,
      message: ''
    }

    this.sendMessage = this.sendMessage.bind(this)
    this.setText = this.setText.bind(this)
  }

  setText(message) {
    this.messageToBeSent.message = message
    this.setState({ message })
  }
  sendMessage() {
    let { accountInfo } = this.props
    if (this.lastMsg == this.messageToBeSent.message) return
    this.messageToBeSent.message = this.messageToBeSent.message.trim()
    if (this.messageToBeSent.message == '') return
    this.lastMsg = this.messageToBeSent.message
    if (accountInfo) this.messageToBeSent.balance = accountInfo.balance
    else this.messageToBeSent.balance = 0

    if (this.messageToBeSent.message != '') {
      Sykiks.sendMessage(this.messageToBeSent)
    }
    Keyboard.dismiss()
    this.setState({ message: '' })
  }

  componentWillReceiveProps(nextProps) {
    let me = this.props.profile.uid

    let from = ''
    try {
      from = _.last(_.values(nextProps.inboxMsgs)).from
    } catch (e) {}

    if (from != '' && from != me) Sykiks.markAsRead(from)
  }

  renderItem(type) {
    return ({ item }) => (
      <ListItem avatar style={styles.profileImage}>
        <Left style={{ alignSelf: 'flex-start' }}>
          <ProfileModalHandler uid={item.from}>
            <ThumbnailByUid uid={item.from} />
          </ProfileModalHandler>
        </Left>
        <Body style={{ borderBottomWidth: 0 }}>
          <ProfileModalHandler uid={item.from}>
            <Text style={styles.profileName}>
              <ScreenNameByUid uid={item.from} />
            </Text>
          </ProfileModalHandler>
          <Text style={styles.profileabout}>{item.message}</Text>
          <Text style={styles.time}>
            {item.msgRate != 0 && (
              <Text style={styles[amountClass[type]]}>
                {Sykiks.formatCurrrecy(item.msgRate)}{' '}
              </Text>
            )}
            <Text> {Sykiks.formatDateandTime(item.time)} </Text>
          </Text>
        </Body>
      </ListItem>
    )
  }

  render() {
    let { inboxMsgs, profile } = this.props
    this.messageToBeSent.msgRate = profile.msgRate
    inboxMsgs = Sykiks.toArray(inboxMsgs)
    return (
      <KeyboardAvoidingView
        style={styles.mainContainer}
        behavior="padding"
        keyboardVerticalOffset={73}>
        <View style={styles.container}>
          {isEmpty(inboxMsgs) && (
            <DefaultText text="Your conversation will be seen here" />
          )}
          {inboxMsgs && (
            <FlatList
              data={inboxMsgs}
              keyExtractor={({ key }) => key}
              renderItem={this.renderItem(profile.type)}
              style={{ height: '85%' }}
              inverted
            />
          )}
          {
            <View style={styles.footer}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                placeholder="Type your message"
                multiline
                onChangeText={this.setText}
                value={this.state.message}
              />
              <Button
                transparent
                light
                style={styles.sendButton}
                onPress={this.sendMessage}>
                <Text style={styles.sendButtonText}>SEND</Text>
              </Button>
            </View>
          }
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  footer: {
    // flex: 1,
    flexDirection: 'row',
    height: 60,
    // paddingTop: 15,
    // paddingBottom: 15,
    borderTopWidth: 0.5,
    borderTopColor: '#B6B6B6'
    // marginTop:10
  },
  input: {
    paddingHorizontal: '5%',
    // paddingVertical: '5%',
    fontSize: 18,
    width: '80%',
    color: '#000'
  },
  profileName: {
    fontWeight: 'bold',
    fontSize: 18
  },
  profileImage: {
    borderBottomWidth: 0.5,
    borderColor: '#c9c9c9',
    marginRight: 17,
    paddingVertical: 5
  },
  profileabout: {
    fontSize: 12,
    lineHeight: 17
  },
  time: {
    fontSize: 12,
    marginTop: 10,
    color: colors.greyText
  },
  sendButton: {
    width: '20%',
    paddingHorizontal: '3%'
  },
  sendButtonText: {
    fontWeight: 'bold',
    color: colors.purple_icon
  },
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  greenAmount: {
    alignSelf: 'flex-start',
    color: colors.green
  },
  redAmount: {
    alignSelf: 'flex-start',
    color: 'red'
  }
})
