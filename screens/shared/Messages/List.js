import React from 'react'
import { StyleSheet, Text, FlatList } from 'react-native'
import { Content, ListItem, Left, Body, Right } from 'native-base'
import { colors, easyFirebase, Sykiks } from '@services'

import { isEmpty } from 'react-redux-firebase'
import { DefaultText, ThumbnailByUid, ScreenNameByUid } from '@shared'

let MessagesList = ({ inboxList, navigation: { navigate }, unread }) => {
  if (isEmpty(inboxList))
    return <DefaultText text="Your messages will be seen here" />
  if (!unread) unread = {}

  let inboxListArr = []
  if (inboxList) inboxListArr = Sykiks.toArray(inboxList)

  return (
    <Content style={{ backgroundColor: colors.white }}>
      <FlatList
        data={inboxListArr}
        keyExtractor={({ key }) => key}
        extraData={unread}
        renderItem={({ item }) => {
          return (
            <ListItem
              avatar
              style={styles.profileImage}
              onPress={() => {
                navigate('Inbox', item)
              }}>
              <Left>
                <ThumbnailByUid uid={item.from} />
              </Left>
              <Body style={{ borderBottomWidth: 0, width: '70%' }}>
                <Text
                  style={[
                    styles.profileName,
                    {
                      fontFamily: unread[item.from] ? 'Lato_bold' : 'Lato',
                      fontWeight: unread[item.from] ? '900' : 'normal'
                    }
                  ]}>
                  <ScreenNameByUid uid={item.from} />
                </Text>
                <Text
                  note
                  style={{
                    fontSize: 11,
                    fontFamily: unread[item.from] ? 'Lato_bold' : 'Lato',
                    fontWeight: unread[item.from] ? '900' : 'normal'
                  }}>
                  {Sykiks.truncate(item.lastMessage)}
                </Text>
              </Body>
              <Right style={{ borderBottomWidth: 0, width: '25%' }}>
                <Text style={{ fontSize: 10 }}>
                  {Sykiks.formatDateandTime(item.time)}{' '}
                </Text>
              </Right>
            </ListItem>
          )
        }}
      />
    </Content>
  )
}

export default easyFirebase(['inboxList', 'profile', '@unread'])(MessagesList)

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    marginTop: 25
  },
  headerImage: {
    width: 140,
    height: 40
  },
  profileName: {
    color: '#764095'
  },
  profileImage: {
    borderBottomWidth: 0.5,
    borderColor: '#c9c9c9',
    marginRight: 17,
    paddingVertical: 10
  }
})
