import React, { Component } from 'react'
import { Text, StyleSheet, TextInput } from 'react-native'
import { Content, Button, View, Spinner } from 'native-base'
import {
  colors,
  easyFirebase,
  MasterStyles,
  Sykiks,
  notification
} from '@services'

@easyFirebase(['profile', '@account'])
export default class ChangePassword extends Component {
  state = {
    currentPassword: '',
    newPassword: '',
    confirmPassword: '',
    isLoading: false
  }

  reauthenticate = () => {
    var user = this.props.firebase.auth().currentUser
    var currentPassword = this.state.currentPassword
    var cred = this.props.firebase.auth.EmailAuthProvider.credential(
      user.email,
      currentPassword
    )
    return user.reauthenticateWithCredential(cred)
  }

  changePassword = () => {
    this.setState({ isLoading: true })
    if (this.state.currentPassword == '') {
      this.setState({ isLoading: false })
      return notification.showToast('Please Enter current Password ')
    }
    if (this.state.newPassword == '') {
      this.setState({ isLoading: false })
      return notification.showToast('Please Enter new Password ')
    }
    if (this.state.confirmPassword == '') {
      this.setState({ isLoading: false })
      return notification.showToast('Please Enter confirm Password ')
    }
    if (this.state.newPassword != this.state.confirmPassword) {
      this.setState({ isLoading: false })
      return notification.showToast(
        'New Password and confirm password should be equal.'
      )
    }
    this.reauthenticate(this.state.currentPassword)
      .then(() => {
        var user = this.props.firebase.auth().currentUser
        user
          .updatePassword(this.state.newPassword)
          .then(() => {
            notification.showToast('Password Changed Successfully.')
            this.setState({ currentPassword: '' })
            this.setState({ newPassword: '' })
            this.setState({ confirmPassword: '' })
            if (Sykiks.profile.type == 'client')
              this.props.navigation.navigate('UpdateClientProfile')
            else this.props.navigation.navigate('UpdatePsychicProfile')
          })
          .catch(error => {
            notification.showToast(error.message)
          })
      })
      .catch(() => {
        notification.showToast('Please check your current password')
      })
    this.setState({ isLoading: false })
  }

  render() {
    return (
      <Content style={MasterStyles.whiteBackground}>
        <View style={styles.bar}>
          <Text style={styles.headerTitle}>Change Password</Text>
        </View>
        <View style={styles.bar}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.balanceTitle}>Old Password</Text>
            <TextInput
              style={styles.values}
              underlineColorAndroid="rgba(0,0,0,0)"
              secureTextEntry
              onChangeText={text => {
                this.setState({ currentPassword: text })
              }}
            />
          </View>
        </View>
        <View style={styles.bar}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.balanceTitle}>New Password</Text>
            <TextInput
              style={styles.values}
              underlineColorAndroid="rgba(0,0,0,0)"
              secureTextEntry
              onChangeText={text => {
                this.setState({ newPassword: text })
              }}
            />
          </View>
        </View>
        <View style={styles.bar}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.balanceTitle}>Confirm Password</Text>
            <TextInput
              style={styles.values}
              underlineColorAndroid="rgba(0,0,0,0)"
              secureTextEntry
              onChangeText={text => {
                this.setState({ confirmPassword: text })
              }}
            />
          </View>
        </View>
        <View style={styles.flexRow}>
          {this.state.isLoading ? (
            <Button style={styles.btn}>
              <Spinner color="#fff" />
            </Button>
          ) : (
            <Button style={styles.btn}>
              <Text
                style={{ color: colors.white }}
                onPress={this.changePassword}>
                UPDATE
              </Text>
            </Button>
          )}
        </View>
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.lightestgrey,
    paddingVertical: 10
  },
  balanceTitle: {
    color: colors.GreyTitle,
    marginHorizontal: 10,
    marginVertical: 7,
    textAlign: 'right',
    fontSize: 12,
    width: '30%'
  },
  values: {
    fontSize: 14,
    height: 50,
    margin: 10,
    marginTop: -5,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    backgroundColor: '#FBFBFB',
    width: '55%',
    padding: 10
  },
  headerTitle: {
    marginVertical: 7,
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 16
  },
  btn: {
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    width: '30%',
    marginLeft: '35%',
    backgroundColor: colors.purple_icon,
    height: 40
  }
})
