import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { View } from 'native-base'
import { colors, Sykiks } from '@services'
import { ProfileModalHandler, ThumbnailByUid } from '@shared'

export default props => {
  return (
    <View style={styles.mainContainer}>
      <View style={{ borderRadius: 50, overflow: 'hidden' }}>
        <ProfileModalHandler uid={props.from}>
          <ThumbnailByUid uid={props.from} style={styles.chatImage} />
        </ProfileModalHandler>
      </View>
      <View style={styles.textComponent}>
        <Text style={styles.chatText}> {props.message} </Text>
        <View>
          <Text style={styles.chatTime}> {Sykiks.formatTime(props.time)} </Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 5
  },
  chatImage: {
    width: 50,
    height: 53
  },
  textComponent: {
    backgroundColor: colors.purple_icon,
    width: '60%',
    borderRadius: 10,
    alignSelf: 'flex-start',
    marginLeft: 10
  },
  chatText: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    color: colors.white
  },
  chatTime: {
    color: colors.purpleText,
    alignSelf: 'flex-end',
    fontSize: 12,
    paddingRight: 10,
    paddingBottom: 5
  }
})
