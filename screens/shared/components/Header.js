import React from 'react'
import { Image } from 'react-native'
import { images } from '@services'

export default {
  navigationOptions: {
    // headerLeft: null,
    headerTitle: (
      <Image
        source={images['header']}
        style={{
          width: 140,
          height: 40
        }}
      />
    ),
    gesturesEnabled: false,
    // headerTintColor: '#FFF',
    headerStyle: {
      // backgroundColor: '#FFF',
      // borderBottomColor: 'transparent',
      borderWidth: 0,
      elevation: 0,
      shadowOpacity: 0,
      paddingLeft: 15,
      paddingRight: 15
    }
  }
}
