import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { View } from 'native-base'
import { colors, Sykiks } from '@services'

export default props => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.textComponent}>
        <Text style={styles.chatText}> {props.message} </Text>
        <View>
          <Text style={styles.chatTime}> {Sykiks.formatTime(props.time)} </Text>
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  mainContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 5
  },
  textComponent: {
    backgroundColor: colors.chatGrey,
    width: '60%',
    borderRadius: 10,
    alignSelf: 'flex-end',
    marginRight: 10
  },
  chatText: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    color: colors.chatTextGrey
  },
  chatTime: {
    color: '#A6A6A6',
    alignSelf: 'flex-end',
    fontSize: 12,
    paddingRight: 10,
    paddingBottom: 5
  }
})
