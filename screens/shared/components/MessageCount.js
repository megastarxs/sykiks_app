import React from 'react'
import _ from 'lodash'
import { View, Text, StyleSheet } from 'react-native'
import { colors, easyFirebase } from '@services'
import { Ionicons } from '@expo/vector-icons'

@easyFirebase(['@unread'])
export default class UnreadCount extends React.Component {
  render() {
    let { unread, focused } = this.props
    let notificationCount = false
    if (unread) notificationCount = _.values(unread).length
    return (
      <View style={styles.mainContainer}>
        <Text>
          <Ionicons
            name={focused ? 'ios-mail' : 'ios-mail-outline'}
            size={24}
            color="#5A5454"
          />
        </Text>
        {notificationCount && (
          <Text style={styles.badge}>{notificationCount}</Text>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  badge: {
    color: 'white',
    position: 'absolute',
    left: 6,
    top: -6,
    backgroundColor: colors.purple_icon,
    borderRadius: 8,
    width: 'auto',
    paddingHorizontal: 5,
    height: 16,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    overflow: 'hidden'
  }
})
