import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { View } from 'native-base'
import { colors } from '@services'

let amount = [15, 50, 100]

export default props => {
  return (
    <View style={styles.container}>
      {amount.map(d => (
        <View key={d}>
          <TouchableOpacity
            style={styles.amountContainer}
            onPress={() => {
              props.addFunds(d)
            }}>
            <Text style={styles.amount}>${d}</Text>
            <Text style={styles.credit}>credit</Text>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  amount: {
    color: '#7CB031',
    fontWeight: 'bold',
    fontSize: 16
  },
  container: {
    flexDirection: 'row',
    margin: 0
  },
  amountContainer: {
    flexDirection: 'column',
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#7CB031',
    paddingVertical: 10,
    paddingHorizontal: 25,
    margin: 10,
    alignItems: 'center'
  },
  credit: {
    color: colors.greyText,
    fontSize: 10
  }
})
