import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { View } from 'native-base'
import { colors, easyFirebase, Sykiks } from '@services'
import { Ionicons } from '@expo/vector-icons'
import { ScreenNameByUid } from '@shared'

@easyFirebase(['profile'])
export default class PsychicCard extends React.Component {
  render() {
    let { message, rating, postedOn, from } = this.props
    return (
      <View>
        <View style={{ marginVertical: 10, marginHorizontal: 0 }}>
          <Text style={styles.description}> {message} </Text>
          <Text>
            {rating == 'up' && (
              <Ionicons
                name="ios-thumbs-up"
                size={40}
                style={styles.thumbIconUp}
              />
            )}
            {rating == 'down' && (
              <Ionicons
                name="ios-thumbs-down"
                size={40}
                style={styles.thumbIconDown}
              />
            )}
            <Text style={styles.name}>
              {' '}
              <ScreenNameByUid uid={from} />{' '}
            </Text>
            <Text style={styles.date}>{Sykiks.formatDate(postedOn)}</Text>
          </Text>
          {this.props.children}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  description: {
    color: colors.lightTextgrey,
    fontSize: 16,
    marginTop: 10,
    lineHeight: 24
  },
  thumbIconUp: {
    color: colors.green
  },
  thumbIconDown: {
    color: '#EB6161'
  },
  name: {
    fontWeight: 'bold',
    color: colors.lightTextgrey,
    fontSize: 16
  },
  date: {
    color: colors.lightTextgrey,
    fontSize: 14
  }
})
