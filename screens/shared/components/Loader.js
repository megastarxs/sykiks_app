import React from 'react'
import { Spinner } from 'native-base'
import { colors } from '@services'

export default () => (
  <Spinner color={colors.purple} style={{ flex: 1, alignSelf: 'center' }} />
)
