import React from 'react'

import { TouchableOpacity } from 'react-native'
import { easyFirebase } from '@services'

let ProfileModalHandler = ({ children, uid, updateProfileModalId }) => (
  <TouchableOpacity onPress={() => updateProfileModalId(uid)}>
    {children}
  </TouchableOpacity>
)

export default easyFirebase(['profile'])(ProfileModalHandler)
