import React, { Fragment } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { Sykiks } from '@services'

const enhance = compose(
  firebaseConnect(({ uid }) => [
    { path: `users/${uid}`, storeAs: `users_${uid}` }
  ]),
  connect(({ firebase: { data } }, { uid }) => ({
    profile: data[`users_${uid}`]
  }))
)

const ScreenName = ({ profile }) => (
  <Fragment> {profile ? profile.screenName : null} </Fragment>
)
const MemberSinceContainer = ({ profile }) => (
  <Fragment>{profile ? Sykiks.formatDate(profile.createdAt) : null}</Fragment>
)

export const MemberSinceByUid = enhance(MemberSinceContainer)
export default enhance(ScreenName)
