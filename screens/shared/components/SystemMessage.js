import React from 'react'
import { StyleSheet, Text } from 'react-native'
import { View } from 'native-base'
import { colors } from '@services'
import { Entypo } from '@expo/vector-icons'

export default ({ message }) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.textComponent}>
        <Text style={styles.chatText}>
          <Text style={{ color: '#686868' }}>
            {' '}
            <Entypo
              name="info-with-circle"
              size={15}
              style={{ color: '#ABABAB', paddingRight: 10 }}
            />{' '}
            {message}
          </Text>
        </Text>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  mainContainer: {
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10
  },
  textComponent: {
    // backgroundColor: colors.chatGrey,
    // width: '60%',
    borderRadius: 10,
    alignSelf: 'center',
    marginRight: 10
  },
  chatText: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    color: colors.chatTextGrey
  }
})
