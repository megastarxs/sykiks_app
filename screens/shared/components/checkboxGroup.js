/**
 * Checkbox group
 * ataomega@gmail.com
 * www.atasmohammadi.net
 * version 1.0
 */
import React, { Component } from 'react'
import { Text, View, Dimensions } from 'react-native'
import { Button } from 'native-base'
import { colors } from '@services'

export default class CheckboxGroup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pageWidth: Dimensions.get('window').width,
      pageHeight: Dimensions.get('window').height,
      selected: []
    }
  }

  getNewDimensions(event) {
    var pageHeight = event.nativeEvent.layout.height
    var pageWidth = event.nativeEvent.layout.width
    this.setState({
      pageHeight,
      pageWidth
    })
  }

  render() {
    const { checkboxes, callback } = this.props

    return (
      <View
        onLayout={evt => {
          this.getNewDimensions(evt)
        }}
        style={{
          flex: 1,
          // flexDirection: rowDirection,
          padding: 5
        }}>
        {checkboxes.map((checkbox, index) => {
          return (
            <Button
              key={index}
              block
              onPress={() => {
                callback(index)
              }}
              style={{
                marginBottom: 5,
                borderColor: colors.purple,
                backgroundColor: checkbox.selected
                  ? colors.purple_icon
                  : '#fff',
                borderWidth: 1,
                padding: 5,
                borderRadius: 3
              }}>
              <Text
                style={{
                  color: !checkbox.selected ? colors.purple_icon : '#fff'
                }}>
                {checkbox.label}
              </Text>
            </Button>
          )
        })}
      </View>
    )
  }
}
