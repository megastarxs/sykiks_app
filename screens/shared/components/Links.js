import React from 'react'
import { StyleSheet, Text, Linking, TouchableOpacity } from 'react-native'
import { View } from 'native-base'
import { colors, config } from '@services'

export default props => {
  return (
    <View>
      <View>
        <Text style={styles.Title}>MORE INFORMATION</Text>
      </View>
      <View style={styles.bar}>
        <TouchableOpacity
          onPress={() => Linking.openURL(config.privacyPolicyUrl)}>
          <Text
            style={{
              marginBottom: 15,
              marginTop: 15,
              marginHorizontal: 15,
              color: colors.purple_icon
            }}>
            Privacy Policy
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bar}>
        <TouchableOpacity onPress={() => Linking.openURL(config.aboutusUrl)}>
          <Text
            style={{
              marginBottom: 15,
              marginTop: 15,
              marginHorizontal: 15,
              color: colors.purple_icon
            }}>
            About Us
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bar}>
        <TouchableOpacity onPress={() => Linking.openURL(config.termsUrl)}>
          <Text
            style={{
              marginBottom: 15,
              marginTop: 15,
              marginHorizontal: 15,
              color: colors.purple_icon
            }}>
            Terms
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bar}>
        <TouchableOpacity onPress={() => Linking.openURL(config.websiteUrl)}>
          <Text
            style={{
              marginBottom: 15,
              marginTop: 15,
              marginHorizontal: 15,
              color: colors.purple_icon
            }}>
            Website
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bar}>
        <Text style={{ margin: 15 }}>Version</Text>
        <Text style={{ marginLeft: 'auto', marginRight: 25 }}>
          {config.version}
        </Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  Title: {
    backgroundColor: '#F7F7F7',
    padding: 15,
    color: colors.greyText
  },
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.lightestgrey,
    alignItems: 'center'
  }
})
