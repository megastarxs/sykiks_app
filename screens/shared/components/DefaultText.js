import React from 'react'
import { View, Text } from 'react-native'

export default props => {
  var defaultText = 'No items available'
  if (!props.style) props.style = {}
  return (
    <View
      style={{ alignItems: 'center', justifyContent: 'center', height: '85%' }}>
      <Text
        style={[
          { fontSize: 20, fontStyle: 'italic', paddingTop: '20%' },
          props.style
        ]}>
        {' '}
        {props.text || defaultText}{' '}
      </Text>
    </View>
  )
}
