import React from 'react'
import _ from 'lodash'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect } from 'react-redux-firebase'
import { images } from '@services'
import { Image } from 'react-native'
import { Thumbnail } from 'native-base'

const enhance = compose(
  firebaseConnect(({ uid }) => [
    { path: `images/${uid}`, storeAs: `image_${uid}` }
  ]),
  connect(({ firebase: { data } }, { uid }) => ({
    src: data[`image_${uid}`]
  }))
)

let defaultImage = images['user-default']

const ThumbnailP = props => (
  <Thumbnail
    source={{ uri: props.src || defaultImage }}
    {..._.pick(props, ['resizeMode', 'style'])}
  />
)

const ImageP = props => (
  <Image
    source={{ uri: props.src || defaultImage }}
    {..._.pick(props, ['resizeMode', 'style'])}
  />
)

export const ThumbnailByUid = enhance(ThumbnailP)
export const ImageByUid = enhance(ImageP)
