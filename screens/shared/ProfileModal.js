import React, { Component, Fragment } from 'react'
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TextInput
} from 'react-native'
import { Container, Header, Button, Body, Icon } from 'native-base'
import {
  images,
  colors,
  easyFirebase,
  Sykiks,
  MasterStyles,
  notification
} from '@services'
import { ImageByUid, MemberSinceByUid, ScreenNameByUid } from '@shared'
var { height, width } = Dimensions.get('window')

@easyFirebase(['profile', 'profileModal'])
export default class ProfileModal extends Component {
  state = {
    reporting: false,
    blocking: false,
    reportReason: ''
  }

  reportUser() {
    let { updateProfileModalId, profileModal } = this.props
    let { reporting } = this.state
    if (reporting) {
      Sykiks.reportUser({
        reportedId: profileModal.uid,
        reason: this.reportReason
      })
      this.setState({ reporting: false })
      updateProfileModalId(false)
      notification.showToast(
        'Thanks for reporting! Your report will be reviewed.'
      )
    } else {
      this.setState({ reporting: true })
    }
  }

  blockUser() {
    let { updateProfileModalId, profileModal } = this.props
    Sykiks.blockUser(profileModal.uid)
    updateProfileModalId(false)
    notification.showToast('User blocked')
  }
  render() {
    let { updateProfileModalId, profile, profileModal } = this.props
    if (!profileModal.uid) return null
    let { reporting } = this.state

    return (
      <Container style={styles.content}>
        <Header style={styles.header}>
          <Body style={styles.headerBody}>
            <Image
              source={images['header']}
              resizeMode="cover"
              style={styles.headerImage}
            />
          </Body>
        </Header>
        <View style={{ justifyContent: 'center', alignSelf: 'stretch' }}>
          <View>
            <View style={{ alignSelf: 'flex-end', padding: 15 }}>
              <Button
                onPress={() => {
                  updateProfileModalId(false)
                  this.setState({ reporting: false })
                }}
                style={{ backgroundColor: colors.purple_icon }}>
                <Icon name="ios-close-circle" />
              </Button>
            </View>
            <View style={styles.logoContainer}>
              <ImageByUid
                uid={profileModal.uid}
                style={styles.logo}
                resizeMode="cover"
              />
              <Text style={styles.nameTextItem}>
                <ScreenNameByUid uid={profileModal.uid} />
              </Text>
              <Text style={styles.textItem}>
                Member since:
                <MemberSinceByUid uid={profileModal.uid} />
              </Text>
            </View>
          </View>
          {reporting && (
            <View>
              <TextInput
                multiline
                style={styles.about}
                placeholder="Enter Reason Heres"
                underlineColorAndroid="rgba(0,0,0,0)"
                onChangeText={reportReason =>
                  (this.reportReason = reportReason.trim())
                }
              />
            </View>
          )}

          {profile.uid != profileModal && (
            <View style={styles.btnWrapper}>
              <Button
                style={[MasterStyles.btn, styles.purpleButton]}
                onPress={this.reportUser.bind(this)}>
                <Text style={styles.whiteBtnText}>Report</Text>
              </Button>
              {!reporting && (
                <Fragment>
                  <Button
                    style={[MasterStyles.btn, styles.button]}
                    onPress={this.blockUser.bind(this)}>
                    <Text style={styles.purpleBtnText}>Block</Text>
                  </Button>
                </Fragment>
              )}
            </View>
          )}
        </View>
      </Container>
    )
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    marginTop: 15
  },
  content: {
    flex: 1,
    backgroundColor: '#fff',
    position: 'absolute',
    height,
    width
  },
  headerImage: {
    width: 140,
    height: 40,
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  logoContainer: {
    // flex: 1,
    borderWidth: 10,
    borderColor: colors.white
  },
  logo: {
    width: 120,
    height: 120,
    alignItems: 'stretch',
    alignSelf: 'center'
    // flex: 1,
  },
  btnWrapper: {
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'row'
  },
  purpleBtnText: {
    color: colors.purple_icon
  },
  whiteBtnText: {
    color: colors.white
  },
  textItem: {
    textAlign: 'center',
    margin: '2%'
  },
  nameTextItem: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: '2%'
  },
  button: {
    backgroundColor: colors.light_purple_button,
    justifyContent: 'center',
    width: '30%',
    margin: '2%',
    paddingTop: 0
  },
  purpleButton: {
    backgroundColor: colors.purple,
    justifyContent: 'center',
    width: '30%',
    margin: '2%',
    paddingTop: 0
  },
  headerBody: {
    flexDirection: 'row'
  },
  backArrow: {
    margin: 16
  },
  about: {
    marginVertical: 7,
    marginLeft: 15,
    color: colors.grey,
    lineHeight: 22,
    fontSize: 14,
    width: width - 50,
    paddingLeft: 10,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    height: 80,
    backgroundColor: '#FBFBFB',
    borderRadius: 10
  }
})
