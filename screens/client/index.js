import React from 'react'
import { TabNavigator } from 'react-navigation'
import { Ionicons } from '@expo/vector-icons'

import { colors } from '@services'

import Psychics from './Psychics'
import Messages from '../shared/Messages'
import SessionHistory from '../shared/SessionHistory'
import Settings from './Settings'
import { MessageCount } from '@shared'

export default TabNavigator(
  {
    Psychics: {
      screen: Psychics,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name={focused ? 'ios-contacts' : 'ios-contacts-outline'}
            size={24}
            color="#5A5454"
          />
        )
      }
    },
    Messages: {
      screen: Messages,
      navigationOptions: {
        tabBarIcon: ({ focused }) => <MessageCount focused={focused} />
      }
    },
    SessionHistory: {
      screen: SessionHistory('client'),
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name={focused ? 'ios-chatboxes' : 'ios-chatboxes-outline'}
            size={24}
            color="#5A5454"
          />
        )
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name={focused ? 'ios-settings' : 'ios-settings-outline'}
            size={24}
            color="#5A5454"
          />
        )
      }
    }
  },
  {
    tabBarPosition: 'bottom',
    backBehavior: 'none',
    initialRouteName: 'Psychics',
    swipeEnabled: false,
    lazy: true,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      style: {
        backgroundColor: colors.white
      },
      indicatorStyle: {
        backgroundColor: 'white'
      }
    }
  }
)
