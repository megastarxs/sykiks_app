import { StackNavigator } from 'react-navigation'
import { headerProps } from '@shared'
import UpdateClientProfile from './UpdateClientProfile'
import AddFunds from './AddFunds'
import ChangePassword from '../../shared/ChangePassword'

export default StackNavigator(
  {
    UpdateClientProfile: {
      screen: UpdateClientProfile,
      navigationOptions: ({ navigation }) => ({})
    },
    AddFunds: {
      screen: AddFunds,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: false
      })
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: ({ navigation }) => ({})
    }
  },
  {
    ...headerProps,
    initialRouteName: 'UpdateClientProfile'
  }
)
