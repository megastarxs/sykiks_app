import React, { Component } from 'react'
import {
  findNodeHandle,
  NativeModules,
  Text,
  StyleSheet,
  TouchableOpacity,
  Keyboard
} from 'react-native'
import { Content, Button, View, Input } from 'native-base'
import {
  colors,
  easyFirebase,
  MasterStyles,
  Sykiks,
  notification
} from '@services'
import { ImagePicker } from 'expo'
import { Links, Funds, ThumbnailByUid } from '@shared'
import Popover from 'react-native-modal-popover'
@easyFirebase(['profile', '@accountInfo'])
export default class UpdateClientProfile extends Component {
  state = {
    showPopover: false,
    popoverAnchor: { x: 0, y: 0, width: 0, height: 0 }
  }
  profile = {}
  setButton = e => {
    const handle = findNodeHandle(this.button)
    if (handle) {
      NativeModules.UIManager.measure(handle, (x0, y0, width, height, x, y) => {
        this.setState({ popoverAnchor: { x, y, width, height } })
      })
    }
  }

  openPopover = () => {
    this.setState({ showPopover: true })
  }

  closePopover = () => this.setState({ showPopover: false })

  _pickImage = async () => {
    let { firebase } = this.props
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    })

    if (result.cancelled) return

    result.base64 = 'data:image/jpeg;base64,' + result.base64
    var image = result.base64
    let obj = {}
    obj[this.props.profile.uid] = image
    firebase.ref(`images`).update(obj)
  }

  update() {
    let { firebase } = this.props
    let profile = {}
    let arr = ['screenName']

    if (!this.props.profile.screenName) {
      if (this.profile.screenName == '')
        return notification.showToast('Please Enter Screen Name ')
    }

    arr.forEach(d => {
      if (this.profile[d] && this.profile[d] != this.props.profile[d])
        profile[d] = this.profile[d]
    })
    Keyboard.dismiss()
    firebase.updateProfile(profile)
    notification.showToast('Profile updated')
  }

  addFunds(d) {
    let { navigate } = this.props.navigation
    this.closePopover()
    navigate('AddFunds', d)
  }

  render() {
    let { navigate } = this.props.navigation
    let { firebase } = this.props
    let account = this.props.accountInfo || {}

    let profile = {
      screenName: '',
      aboutme: 'Unknown',
      email: firebase.auth().currentUser.email,
      balance: account.balance || 0,
      ...this.props.profile
    }

    return (
      <Content style={MasterStyles.whiteBackground}>
        <View style={styles.headerContent}>
          <View style={styles.logoContainer}>
            <TouchableOpacity onPress={this._pickImage.bind(this)}>
              <ThumbnailByUid uid={profile.uid} style={styles.logo} />
            </TouchableOpacity>
          </View>

          <View style={styles.profileNameView}>
            <Text style={styles.profileName}>
              {' '}
              {profile.screenName}{' '}
              {/* <MaterialCommunityIcons name='pencil-circle-outline' style={styles.editIcon}></MaterialCommunityIcons> */}
            </Text>
          </View>
        </View>
        <View>
          <Text
            style={{
              backgroundColor: '#F7F7F7',
              padding: 15,
              color: colors.greyText
            }}>
            ACCOUNT INFORMATION
          </Text>
        </View>
        <View style={[styles.bar, { borderBottomWidth: 0 }]}>
          <View style={styles.leftBar}>
            <Text style={styles.balanceTitle}>Balance</Text>
            <Text style={styles.balance}>
              {Sykiks.formatCurrrecy(profile.balance)}
            </Text>
          </View>
        </View>
        <View style={styles.bar}>
          <View style={styles.rightBar}>
            {/* <Button
              style={[
                styles.btnStyle,
                { backgroundColor: colors.purple_icon }
              ]}>
              <Text style={styles.buttonText}>TRANSACTIONS</Text>
            </Button> */}
            <Button
              style={[styles.btnStyle, { backgroundColor: colors.darkgreen }]}
              ref={r => {
                this.button = r
              }}
              onPress={this.openPopover}
              onLayout={this.setButton}>
              <Text style={styles.buttonText}>ADD FUNDS</Text>
            </Button>
            <Popover
              arrowStyle={{ display: 'none' }}
              contentStyle={{ marginTop: -30 }}
              visible={this.state.showPopover}
              fromRect={this.state.popoverAnchor}
              onClose={this.closePopover}
              placement="top">
              <Funds addFunds={this.addFunds.bind(this)} />
            </Popover>
          </View>
        </View>
        <View style={styles.bar}>
          <View style={{ flexDirection: 'row', marginLeft: 5 }}>
            <Text style={{ color: colors.GreyTitle, padding: 15 }}>
              Screen Name
            </Text>
            <Input
              defaultValue={profile.screenName}
              style={{ padding: 10 }}
              onChangeText={screenName =>
                (this.profile.screenName = screenName)
              }
            />
          </View>
        </View>
        <View style={styles.bar}>
          <View style={{ flexDirection: 'row', marginLeft: 5 }}>
            <Text style={{ color: colors.GreyTitle, padding: 15 }}>Email</Text>
            <Text style={{ padding: 15 }}>{profile.email}</Text>
          </View>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              navigate('ChangePassword')
            }}>
            <Text
              style={{
                marginBottom: 15,
                marginTop: 15,
                marginHorizontal: 20,
                color: colors.purple_icon
              }}>
              Change Password
            </Text>
          </TouchableOpacity>
        </View>
        <Links />
        <View style={styles.flexRow}>
          <Button style={styles.btn} onPress={this.update.bind(this)}>
            <Text style={{ color: colors.white }}>UPDATE</Text>
          </Button>
          <Button style={styles.btn} onPress={() => firebase.logout()}>
            <Text style={{ color: colors.white }}>LOGOUT</Text>
          </Button>
        </View>
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  headerContent: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 15,
    backgroundColor: colors.backgroundGrey,
    paddingTop: '10%'
  },
  profileNameView: {
    margin: 15,
    alignItems: 'center'
  },
  profileName: {
    fontSize: 24,
    color: colors.white,
    fontWeight: 'bold'
  },
  aboutProfile: {
    marginTop: 5,
    color: colors.white
  },
  logoContainer: {
    alignItems: 'stretch',
    flex: 1,
    borderWidth: 10,
    borderColor: colors.white
  },
  logo: {
    // flex:1,
    width: 200,
    height: 200,
    borderRadius: 0
    // width: 200,
    // height: 200,
  },

  btn: {
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    width: '30%',
    marginHorizontal: '10%',
    backgroundColor: colors.purple_icon,
    height: 40
  },
  profileImageContainer: {
    alignItems: 'center',
    marginBottom: '40%'
  },
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.lightestgrey,
    alignItems: 'center'
  },
  balanceTitle: {
    color: colors.GreyTitle,
    // marginTop: 7,
    padding: 15
    // marginBottom : 5
  },
  balance: {
    // margin: 7
    padding: 15
  },
  btnStyle: {
    marginHorizontal: 5,
    marginVertical: 10,
    padding: 7,
    borderRadius: 5,
    height: 35,
    width: '47%',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  buttonText: {
    color: colors.white,
    fontSize: 10
  },
  leftBar: {
    // width: '30%',
    flexDirection: 'row',
    marginLeft: 5
  },
  rightBar: {
    // width: '70%',
    flexDirection: 'row',
    marginLeft: 5
  },
  flexRow: {
    flexDirection: 'row'
  }
})
