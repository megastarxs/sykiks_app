import React, { Component } from 'react'
import { WebView } from 'react-native'
import { config, notification, easyFirebase } from '@services'

function getQuery(url) {
  var qs = url.substring(url.indexOf('?') + 1).split('&')
  for (var i = 0, result = {}; i < qs.length; i++) {
    qs[i] = qs[i].split('=')
    result[qs[i][0]] = decodeURIComponent(qs[i][1])
  }
  return result
}
@easyFirebase(['profile'])
export default class Addfunds extends Component {
  paid = false
  onNavigationStateChange(d) {
    if (!this.paid && d.url.includes('/payment')) {
      var params = getQuery(d.url)
      if (params.status == 'success') {
        notification.showToast(`$${params.amount} added to your account`)
      } else notification.showToast('Payment failed, please try again')

      this.props.navigation.navigate('UpdateClientProfile')
      this.paid = true
    }
  }
  render() {
    let amount = this.props.navigation.state.params
    let { profile } = this.props
    return (
      <WebView
        source={{ uri: `${config.paymentUrl}/${profile.uid}/${amount}` }}
        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
      />
    )
  }
}
