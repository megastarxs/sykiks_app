import React, { Component } from 'react'
import { Platform, BackHandler } from 'react-native'

import { Content, List } from 'native-base'
import _ from 'lodash'

import Card from './components/Card'
import { easyFirebase, Sykiks } from '@services'

class PsychicList extends Component {
  componentDidMount() {
    if (Platform.OS === 'android')
      BackHandler.addEventListener('hardwareBackPress', () => true)
  }

  render() {
    let {
      psychics,
      favorites,
      accountInfo,
      blocked,
      psychicFilters
    } = this.props

    let { navigate } = this.props.navigation
    let balance = 0
    if (accountInfo && accountInfo.balance) balance = accountInfo.balance

    if (_.isEmpty(favorites)) favorites = {}

    if (_.isEmpty(blocked)) blocked = {}

    let categories = []
    _.each(psychicFilters.categories, (v, k) => {
      if (v) categories.push(k)
    })

    let psychicsList = []
    Object.keys(psychics).forEach(k => {
      let push = false
      let d = psychics[k]
      d.k = k
      d.isFav = favorites[k] || false

      if (blocked[k] || d.banned || d.status == 'pending') return

      d.ratings = Sykiks.totalRating(d)
      d.percentage = Sykiks.percentageRating(d)

      if (!psychicFilters.favorite) push = true
      else if (favorites[k]) push = true

      if (push && !_.isEmpty(categories)) {
        push = false
        categories.forEach(d2 => {
          if (d.categories.includes(d2)) push = true
        })
      }

      if (push && psychicFilters.available) {
        push = false
        if (d.status == 'Available') push = true
      }

      if (push) psychicsList.push(d)
    })

    if (psychicFilters.sortBy == 'Top Psychics')
      psychicsList = _.orderBy(
        psychicsList,
        ['online', 'ratings'],
        ['asc', 'desc']
      )
    if (psychicFilters.sortBy == 'New Psychics')
      psychicsList = _.orderBy(
        psychicsList,
        ['online', 'createdAt'],
        ['asc', 'desc']
      )
    if (psychicFilters.sortBy == 'Price lowest')
      psychicsList = _.orderBy(
        psychicsList,
        ['online', 'chatRate'],
        ['asc', 'desc']
      )
    if (psychicFilters.sortBy == 'Price highest')
      psychicsList = _.orderBy(
        psychicsList,
        ['online', 'chatRate'],
        ['asc', 'desc']
      )

    return (
      <Content>
        <List>
          {psychicsList.map(d => (
            <Card
              key={d.k}
              profile={d}
              isFav={d.isFav}
              navigate={navigate}
              balance={balance}
            />
          ))}
        </List>
      </Content>
    )
  }
}

export default easyFirebase([
  'psychics',
  '@favorites',
  '@accountInfo',
  '@blocked'
])(PsychicList)
