import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Platform,
  BackHandler,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native'

import { Content, Button } from 'native-base'
import { ThumbnailByUid, ScreenNameByUid } from '@shared'
import {
  colors,
  easyFirebase,
  Sykiks,
  MasterStyles,
  notification
} from '@services'
import { Ionicons } from '@expo/vector-icons'

@easyFirebase(['profile'])
export default class Review extends Component {
  state = {
    rating: false
  }

  componentDidMount() {
    if (Platform.OS === 'android')
      BackHandler.addEventListener('hardwareBackPress', () => true)
  }

  submit() {
    let { partnerUid, sessionKey } = this.props.navigation.state.params
    let { profile } = this.props
    let obj = { from: profile.uid }
    if (!this.state.rating)
      notification.showToast('Please choose Up or down rating!')
    if (!this.message)
      notification.showToast('Please Enter appropriate message!')
    if (this.state.rating) obj.rating = this.state.rating
    if (this.message) obj.message = this.message
    if (obj.rating && obj.message) {
      Sykiks.giveReview(partnerUid, obj, sessionKey)
      Keyboard.dismiss()
      this.props.navigation.navigate('PsychicList')
    }
  }

  render() {
    let { partnerUid } = this.props.navigation.state.params
    return (
      <KeyboardAvoidingView style={styles.mainContainer} behavior="padding">
        <Content style={MasterStyles.whiteBackground}>
          <View style={styles.container}>
            <Text style={{ textAlign: 'center', fontSize: 20 }}>
              How was your session with <ScreenNameByUid uid={partnerUid} /> ?
            </Text>

            <ThumbnailByUid uid={partnerUid} style={styles.thumbnail} />

            <View style={styles.buttons}>
              <View style={{ width: '50%', marginLeft: '25%' }}>
                <Button
                  rounded
                  style={[
                    styles.btnUp,
                    this.state.rating == 'up'
                      ? styles.btnActive
                      : { opacity: 0.5 }
                  ]}
                  onPress={() => this.setState({ rating: 'up' })}>
                  <Text style={styles.count}>
                    <Ionicons name="md-thumbs-up" size={26} color="#fff" />{' '}
                  </Text>
                </Button>
              </View>
              <View style={{ width: '50%', marginLeft: '20%' }}>
                <Button
                  rounded
                  style={[
                    styles.btnDown,
                    this.state.rating == 'down'
                      ? styles.btnActive
                      : { opacity: 0.5 }
                  ]}
                  onPress={() => this.setState({ rating: 'down' })}>
                  <Text style={styles.count}>
                    <Ionicons name="md-thumbs-down" size={26} color="#FFF" />
                  </Text>
                </Button>
              </View>
            </View>

            <View style={{ width: '100%', margin: '10%' }}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                placeholder="Type your review message ..."
                multiline
                onChangeText={text => (this.message = text.trim())}
              />
            </View>
            <View>
              <Button
                style={[styles.btn, styles.submitBtn]}
                onPress={this.submit.bind(this)}>
                <Text style={{ color: colors.white }}>SUBMIT</Text>
              </Button>
            </View>
          </View>
        </Content>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.white
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: '15%'
  },
  thumbnail: {
    margin: '10%',
    height: 100,
    width: 100
  },
  count: {
    color: colors.white,
    padding: 5
    // marginLeft: 5,
  },
  buttons: {
    flexDirection: 'row'
  },
  btnActive: {
    opacity: 1
  },
  input: {
    width: '100%',
    height: 100,
    borderWidth: 1,
    borderColor: colors.lightGrey,
    padding: 10,
    borderRadius: 5
  },
  btnUp: {
    backgroundColor: '#65b365',
    paddingHorizontal: 8,
    paddingVertical: 22,
    opacity: 0.5
  },
  btnDown: {
    backgroundColor: '#a53f3d',
    paddingHorizontal: 8,
    paddingVertical: 22,
    opacity: 0.5
  },
  button: {
    borderRadius: 3,
    justifyContent: 'center',
    width: '45%',
    margin: '2%',
    height: 40,
    marginBottom: 5
  },
  submitBtn: {
    backgroundColor: colors.purple_icon
  },
  btn: {
    marginTop: 10,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
})
