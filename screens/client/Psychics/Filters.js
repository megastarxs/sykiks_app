import React, { Component } from 'react'
import { Text } from 'react-native'

import {
  Content,
  List,
  ListItem,
  Body,
  Right,
  Switch,
  Radio
} from 'native-base'

import { connect } from 'react-redux'
import { updatePsychicFilters } from '../../../redux/reducers/psychics-filters'

let categoriesList = [
  'Psychic Readings',
  'Tarot',
  'Spiritual counselling',
  'Astrology',
  'Dream Interpretation',
  'Love and Relationships',
  'Numerology'
]
let sortByList = [
  'Top Psychics',
  'New Psychics',
  'Price lowest',
  'Price highest'
]

@connect(
  ({ psychicFilters }) => ({ psychicFilters }),
  dispatch => ({
    updatePsychicFilters: filters => dispatch(updatePsychicFilters(filters))
  })
)
export default class Filters extends Component {
  render() {
    let { favorite, categories, sortBy, available } = this.props.psychicFilters
    let type = this.props.navigation.state.params
    let { updatePsychicFilters } = this.props

    return (
      <Content>
        {type == 'filter' && (
          <List>
            <ListItem>
              <Body>
                <Text>Available</Text>
              </Body>
              <Right>
                <Switch
                  value={available}
                  onValueChange={() =>
                    updatePsychicFilters({ available: !available })
                  }
                />
              </Right>
            </ListItem>
            <ListItem>
              <Body>
                <Text>Favorites</Text>
              </Body>
              <Right>
                <Switch
                  value={favorite}
                  onValueChange={() =>
                    updatePsychicFilters({ favorite: !favorite })
                  }
                />
              </Right>
            </ListItem>
            <ListItem itemDivider>
              <Text>CATEGORIES</Text>
            </ListItem>
            {categoriesList.map((d, i) => (
              <ListItem key={i}>
                <Body>
                  <Text>{d}</Text>
                </Body>
                <Right>
                  <Switch
                    value={categories[d]}
                    onValueChange={() => {
                      categories[d] = !categories[d]
                      updatePsychicFilters({ categories })
                    }}
                  />
                </Right>
              </ListItem>
            ))}
          </List>
        )}
        {type == 'sort' && (
          <List>
            <ListItem itemDivider>
              <Text>SORT BY</Text>
            </ListItem>
            {sortByList.map((d, i) => (
              <ListItem
                key={i}
                onPress={() => updatePsychicFilters({ sortBy: d })}>
                <Body>
                  <Text>{d}</Text>
                </Body>
                <Right>
                  <Radio selected={d == sortBy} />
                </Right>
              </ListItem>
            ))}
          </List>
        )}
      </Content>
    )
  }
}
