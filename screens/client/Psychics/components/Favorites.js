import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { easyFirebase } from '@services'
import { Ionicons } from '@expo/vector-icons'

@easyFirebase(['@favorites'])
export default class Favorites extends Component {
  addRemoveFav() {
    let { favoritesRef, favorites } = this.props

    let key = this.props.navigation.state.params.k
    let selected = favorites && favorites[key]
    let dbFavoritesRef = favoritesRef.child(key)
    if (selected) dbFavoritesRef.remove()
    else dbFavoritesRef.set(true)
  }

  render() {
    let { favorites } = this.props
    let key = this.props.navigation.state.params.k
    let selected = favorites && favorites[key]

    return (
      <TouchableOpacity onPress={this.addRemoveFav.bind(this)}>
        <Ionicons
          name="md-heart"
          size={32}
          style={{ paddingHorizontal: 10 }}
          color={selected ? '#FA5CE9' : '#9A9A9A'}
        />
      </TouchableOpacity>
    )
  }
}

StyleSheet.create({})
