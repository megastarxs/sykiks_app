import React from 'react'
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native'
import { ListItem, Button, Icon, Left, Body } from 'native-base'
import { colors, Sykiks, notification, Chat } from '@services'
import { ImageByUid } from '@shared'
import { Ionicons, Zocial, MaterialCommunityIcons } from '@expo/vector-icons'
import _ from 'lodash'

let smallText = text => _.truncate(text, { length: 100, separator: ' ' })
const PsychicCard = props => {
  let { navigate, profile, isFav, balance } = props
  profile = {
    chatRate: 'NA',
    msgRate: 'NA',
    balance,
    ...profile
  }

  profile.roomId = Sykiks.clientRoomId(profile.k)

  return (
    <ListItem style={styles.topItem}>
      <Left>
        <View>
          <TouchableOpacity onPress={() => navigate('Profile', profile)}>
            {isFav && (
              <Icon name="ios-heart" size={40} style={styles.heartIcon} />
            )}
            <ImageByUid
              uid={profile.k}
              resizeMode="cover"
              style={styles.profileImage}
            />
          </TouchableOpacity>
          <Text style={styles.ratingContainer}>
            <Icon name="ios-thumbs-up-outline" style={styles.thumbIcon} />
            <Text style={styles.percentage}> {profile.percentage}</Text>
          </Text>
          <View>
            <Text style={styles.rating}>{profile.ratings}</Text>
          </View>
        </View>
        <Body style={styles.bodyList}>
          <TouchableOpacity onPress={() => navigate('Profile', profile)}>
            <Text style={styles.profileName}> {profile.screenName} </Text>
            <Text style={styles.about}> {smallText(profile.introduction)}</Text>
          </TouchableOpacity>
          <View style={styles.bottomItem}>
            {profile.status == 'Available' &&
              profile.online && (
                <Button
                  iconLeft
                  style={[styles.button, { backgroundColor: colors.darkgreen }]}
                  onPress={() => {
                    if (profile.balance >= profile.chatRate)
                      Chat.initClientRequest(profile.k)
                    else notification.showToast('Not enough credits')
                  }}>
                  <MaterialCommunityIcons
                    name="message-text"
                    size={16}
                    color={colors.white}
                  />
                  <Text style={[styles.buttonText, { color: colors.white }]}>
                    Chat now{' '}
                  </Text>
                </Button>
              )}
            {profile.status == 'Available' &&
              !profile.online && (
                <Button
                  iconLeft
                  style={[styles.button, { backgroundColor: colors.orange }]}>
                  <MaterialCommunityIcons
                    name="message-text"
                    size={16}
                    color={colors.white}
                  />
                  <Text style={[styles.buttonText, { color: colors.white }]}>
                    Away{' '}
                  </Text>
                </Button>
              )}
            {(profile.status == 'offline' || !profile.status) && (
              <Button
                iconLeft
                style={[styles.button, { backgroundColor: 'grey' }]}>
                <Ionicons
                  name="ios-close-circle-outline"
                  size={14}
                  color={colors.white}
                />
                <Text style={[styles.buttonText, { color: colors.white }]}>
                  {profile.status || 'Offline'}
                </Text>
              </Button>
            )}
            <Button
              iconLeft
              style={[
                styles.button,
                { backgroundColor: colors.light_purple_button }
              ]}
              onPress={() => navigate('Inbox', profile)}>
              <Zocial name="email" size={14} color={colors.purple_icon} />
              <Text style={[styles.buttonText, { color: colors.purple_icon }]}>
                Message
              </Text>
            </Button>
          </View>
          <View style={styles.bottomItem}>
            <Text style={styles.TextItemsLeft}>
              <Text style={styles.rate}>
                {Sykiks.formatCurrrecy(profile.chatRate)}{' '}
              </Text>
              <Text style={styles.ratePer}>/min</Text>
            </Text>
            <Text style={styles.TextItemsRight}>
              <Text style={styles.ratePerMsg}>
                {Sykiks.formatCurrrecy(profile.msgRate)}{' '}
              </Text>
              <Text style={styles.ratePer}>/msg</Text>
            </Text>
          </View>
        </Body>
      </Left>
    </ListItem>
  )
}
export default PsychicCard
const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    marginTop: 25
  },
  headerImage: {
    width: 140,
    height: 40
  },
  profileName: {
    fontSize: 16,
    color: colors.mildGrey,
    paddingTop: 5
  },
  about: {
    color: colors.mildGrey,
    fontSize: 12,
    paddingBottom: 15,
    minHeight: 60
  },
  rate: {
    color: colors.purpleDark
  },
  ratePerMsg: {
    color: colors.purpleDark,
    paddingLeft: 10
  },
  heartIconTouch: {
    borderWidth: 3,
    borderColor: colors.purple,
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    height: 40,
    backgroundColor: 'transparent',
    borderRadius: 100
  },
  ratePer: {
    color: colors.lightGrey
  },
  heartIcon: {
    position: 'absolute',
    top: 7,
    left: 7,
    zIndex: 99999,
    color: '#F85CA9'
  },
  thumbIcon: {
    color: colors.purple,
    fontWeight: 'bold',
    fontSize: 20,
    marginRight: 10
  },
  rightView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  percentage: {
    fontSize: 15,
    color: colors.darkgreen,
    marginLeft: 5
  },
  rating: {
    alignItems: 'center',
    justifyContent: 'center',
    color: '#7E7E7E',
    textAlign: 'center',
    marginLeft: 0,
    fontSize: 15
  },
  button: {
    borderRadius: 3,
    justifyContent: 'center',
    width: '48%',
    marginRight: '2%',
    paddingLeft: 5,
    height: 25,
    marginBottom: 5
  },
  topItem: {
    marginTop: 10,
    marginBottom: 10
  },
  rowDirection: {
    flexDirection: 'row'
  },
  bottomItem: {
    flexDirection: 'row'
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  buttonText: {
    fontSize: 11,
    paddingLeft: 2,
    paddingRight: 2
  },
  bottomList: {
    paddingTop: 0
  },
  leftItems: {
    alignItems: 'center',
    width: 100
  },
  TextItemsLeft: {
    width: '48%',
    marginRight: '2%',
    paddingLeft: 5,
    alignItems: 'center',
    textAlign: 'center'
  },
  TextItemsRight: {
    width: '48%',
    marginLeft: '2%',
    textAlign: 'center',
    alignItems: 'center'
  },
  profileImage: {
    width: 80,
    height: 80
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'center',
    marginLeft: 0,
    justifyContent: 'center',
    marginTop: 5
  },
  bodyList: {
    paddingLeft: 10
  }
})
