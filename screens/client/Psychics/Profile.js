import React from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  FlatList
} from 'react-native'
import { Content, Button } from 'native-base'
import {
  colors,
  MasterStyles,
  easyFirebase,
  Sykiks,
  notification,
  Chat
} from '@services'
import { Zocial, MaterialCommunityIcons, Ionicons } from '@expo/vector-icons'
import { Review, ProfileModalHandler, ImageByUid } from '@shared'

@easyFirebase(['reviews', '@accountInfo'])
export default class PsychicCard extends React.Component {
  state = {
    active: 'all'
  }

  render() {
    let psychic = this.props.navigation.state.params
    let { reviews, accountInfo } = this.props
    let { active } = this.state
    reviews = Sykiks.toArray(reviews)

    if (reviews[0]) {
      if (active == 'up') reviews = reviews.filter(d => d.rating == 'up')

      if (active == 'down') reviews = reviews.filter(d => d.rating == 'down')
    }

    let { navigate } = this.props.navigation

    let balance = 0
    if (accountInfo && accountInfo.balance) balance = accountInfo.balance

    let profile = {
      chatRate: 'NA',
      msgRate: 'NA',
      ...psychic,
      balance
    }

    return (
      <Content style={MasterStyles.whiteBackground}>
        <View style={styles.headerContent}>
          <View style={styles.profileNameView}>
            <Text style={styles.profileName}> {profile.screenName}</Text>
            <Text style={styles.aboutProfile}>
              {' '}
              Member since: {Sykiks.formatDate(profile.createdAt)}
            </Text>
            {profile.rating_up != 0 ||
              (profile.rating_down != 0 && (
                <Text>
                  <Text style={styles.percentage}> {profile.percentage}</Text>
                  <Text style={styles.ratings}> ( {profile.ratings} )</Text>
                </Text>
              ))}
          </View>
          <View style={styles.logoContainer}>
            <ImageByUid
              uid={profile.k}
              style={styles.logo}
              resizeMode="cover"
            />
          </View>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 5 }}>
          {profile.status == 'Available' ? (
            <Button
              iconLeft
              style={[styles.button, { backgroundColor: colors.darkgreen }]}
              onPress={() => {
                if (profile.balance >= profile.chatRate)
                  Chat.initClientRequest(profile.k)
                else notification.showToast('Not enough credits')
              }}>
              <MaterialCommunityIcons
                name="message-text"
                size={20}
                color={colors.white}
              />
              <Text style={[styles.buttonText, { color: colors.white }]}>
                Chat now{' '}
              </Text>
            </Button>
          ) : (
            <Button
              iconLeft
              style={[styles.button, { backgroundColor: 'grey' }]}>
              <Ionicons
                name="ios-close-circle-outline"
                size={20}
                color={colors.white}
              />
              <Text style={[styles.buttonText, { color: colors.white }]}>
                {profile.status || 'Offline'}
              </Text>
            </Button>
          )}
          <Button
            iconLeft
            style={[
              styles.button,
              { backgroundColor: colors.light_purple_button }
            ]}
            onPress={() => navigate('Inbox', profile)}>
            <Zocial name="email" size={20} color={colors.purple_icon} />
            <Text style={[styles.buttonText, { color: colors.purple_icon }]}>
              MESSAGE
            </Text>
          </Button>
        </View>
        <View style={styles.bottomItem}>
          <Text style={styles.TextItemsLeft}>
            <Text style={styles.rate}>
              {Sykiks.formatCurrrecy(profile.chatRate)}{' '}
            </Text>
            <Text style={styles.ratePer}>per minute</Text>
          </Text>
          <Text style={styles.TextItemsRight}>
            <Text style={styles.ratePerMsg}>
              {Sykiks.formatCurrrecy(profile.msgRate)}{' '}
            </Text>
            <Text style={styles.ratePer}>per message</Text>
          </Text>
        </View>
        <View style={{ marginVertical: 10, marginHorizontal: 20 }}>
          <Text style={[styles.title, { color: colors.lightTextgrey }]}>
            About
          </Text>
          <Text style={styles.description}>
            {profile.aboutme} {'\n'} {profile.introduction}
          </Text>
        </View>
        <View
          style={{
            marginTop: 10,
            paddingBottom: 15,
            marginHorizontal: 20,
            flexDirection: 'row',
            borderBottomWidth: 0.5,
            borderColor: '#B6B6B6'
          }}>
          <Text style={[styles.title, { color: colors.lightTextgrey }]}>
            Specialties
          </Text>
          <Text style={styles.categories}>{profile.categories}</Text>
        </View>
        <View
          style={{
            backgroundColor: colors.white,
            flexDirection: 'row',
            paddingVertical: 15,
            alignSelf: 'flex-end'
          }}>
          <ProfileModalHandler uid={profile.k}>
            <Text
              style={{
                paddingVertical: 10,
                paddingHorizontal: 15,
                color: colors.purple_icon
              }}>
              Report/Block{' '}
            </Text>
          </ProfileModalHandler>
        </View>
        {(profile.rating_up != 0 || profile.rating_down != 0) && (
          <View>
            <View style={styles.reviewMessageContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ active: 'all' })}>
                <Text style={active == 'all' ? styles.activeTab : styles.count}>
                  Reviews ({profile.ratings})
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setState({ active: 'up' })}>
                <Text style={active == 'up' ? styles.activeTab : styles.count}>
                  <Ionicons
                    name="ios-thumbs-up-outline"
                    size={30}
                    color="#7DCD13"
                  />
                  <Text>{profile.rating_up}</Text>
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setState({ active: 'down' })}>
                <Text
                  style={active == 'down' ? styles.activeTab : styles.count}>
                  <Ionicons
                    name="ios-thumbs-down-outline"
                    size={30}
                    color="red"
                  />
                  <Text>{profile.rating_down}</Text>
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {reviews[0] && (
          <View style={{ marginLeft: 15 }}>
            <FlatList
              data={reviews}
              keyExtractor={({ key }) => key}
              ItemSeparatorComponent={() => (
                <View
                  style={{ height: 2, width: '80%', backgroundColor: '#eee' }}
                />
              )}
              renderItem={({ item }) => <Review {...item} />}
            />
          </View>
        )}
      </Content>
    )
  }
}

const styles = StyleSheet.create({
  backArrowIcon: {
    color: '#7E7E7E'
  },
  headerImage: {
    width: 140,
    height: 40
  },
  profileImage: {
    backgroundColor: colors.purple
  },
  profileName: {
    fontSize: 24,
    color: colors.white,
    fontWeight: 'bold'
  },
  aboutProfile: {
    marginTop: 5,
    color: colors.white
  },
  headerContent: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 15,
    backgroundColor: colors.purple_icon
  },
  profileContent: {
    flex: 1,
    alignItems: 'center',
    // alignSelf: 'center',
    width: null,
    padding: 15,
    marginTop: 10,
    backgroundColor: colors.white
  },
  logoContainer: {
    alignItems: 'stretch',
    flex: 1,
    width: 200,
    height: 200,
    borderWidth: 10,
    borderColor: colors.white
  },
  logo: {
    flex: 1
  },
  editIcon: {
    color: colors.white,
    fontSize: 24,
    paddingLeft: 5,
    paddingRight: 5,
    fontWeight: 'bold'
  },
  profileNameView: {
    marginBottom: 5,
    alignItems: 'center'
  },
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: '#AFAFAF',
    alignItems: 'center'
  },
  barTop: {
    color: colors.green,
    fontSize: 16,
    fontWeight: 'bold'
  },
  barTopBlack: {
    color: colors.black,
    fontSize: 16,
    fontWeight: 'bold'
  },
  barBottom: {
    color: '#AFAFAF',
    fontSize: 14,
    fontWeight: 'bold'
  },
  barItem: {
    width: '33%',
    padding: 18,
    alignItems: 'center'
  },
  plusIcon: {
    color: colors.blueIcon
  },
  plusIconContainer: {
    position: 'absolute',
    top: 15,
    right: 10
  },
  messagePanel: {
    padding: 18,
    alignItems: 'center',
    width: '50%'
  },
  messageText: {
    color: colors.black,
    fontSize: 18
  },
  messageCount: {
    position: 'absolute',
    top: 10,
    fontSize: 38,
    fontWeight: 'bold',
    color: colors.purple_icon
  },
  messagePanelRight: {
    padding: 18,
    width: '50%'
  },
  reviewTitle: {
    color: colors.black,
    fontWeight: 'bold'
  },
  reviewIcon: {
    color: colors.white,
    fontSize: 24,
    paddingLeft: 5,
    paddingRight: 5,
    fontWeight: 'bold'
  },
  reviewText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.lightTextgrey
  },

  reviewDetails: {
    color: colors.lightTextgrey
  },
  percentage: {
    color: '#7DCD13',
    marginRight: 5,
    fontWeight: 'bold',
    fontSize: 18
  },
  ratings: {
    color: colors.white,
    marginLeft: 5,

    fontSize: 14
  },
  button: {
    borderRadius: 3,
    justifyContent: 'center',
    width: '45%',
    margin: '2%',
    paddingLeft: 5,
    height: 40,
    marginBottom: 5
  },
  buttonText: {
    paddingLeft: 2,
    paddingRight: 2
  },
  bottomItem: {
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 15,
    borderBottomWidth: 0.5,
    borderColor: '#B6B6B6'
  },
  TextItemsLeft: {
    width: '45%',
    margin: '2%',
    paddingLeft: 5,
    alignItems: 'center',
    textAlign: 'center'
  },
  TextItemsRight: {
    width: '45%',
    margin: '2%',
    textAlign: 'center',
    alignItems: 'center'
  },
  rate: {
    color: '#5D5D5D',
    fontSize: 16,
    fontWeight: 'bold'
  },
  ratePerMsg: {
    color: '#5D5D5D',
    paddingLeft: 10,
    fontSize: 16,
    fontWeight: 'bold'
  },
  ratePer: {
    color: colors.lightGrey
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16
  },

  categories: {
    color: '#8C7D28',
    fontSize: 16,
    lineHeight: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    width: '70%'
  },
  reviewMessageContainer: {
    backgroundColor: colors.purple_icon,
    alignItems: 'center',
    paddingTop: 10,
    flexDirection: 'row'
  },
  activeTab: {
    color: '#4F4F4F',
    alignSelf: 'flex-start',
    backgroundColor: colors.white,
    padding: 15,
    marginLeft: 15,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  count: {
    color: colors.white,
    // alignSelf: 'flex-start',
    padding: 5,
    marginLeft: 5
  }
})
