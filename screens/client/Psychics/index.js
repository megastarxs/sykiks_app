import React from 'react'
import { View } from 'react-native'
import { StackNavigator } from 'react-navigation'
import { Feather, Ionicons } from '@expo/vector-icons'
import { colors } from '@services'

import List from './List'
import Profile from './Profile'

import { headerProps } from '@shared'
import Favorites from './components/Favorites'
import Filters from './Filters'
import ReviewForm from './ReviewForm'

import Chat from './../../shared/Messages/Chat'

export default StackNavigator(
  {
    PsychicList: {
      screen: List,
      navigationOptions: ({ navigation }) => ({
        headerLeft: null,
        headerRight: (
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Ionicons
              name="ios-arrow-round-up"
              size={40}
              onPress={() => navigation.navigate('Filters', 'sort')}
              style={{ marginTop: 6, color: colors.purple_icon }}
            />
            <Ionicons
              name="ios-arrow-round-down"
              size={40}
              onPress={() => navigation.navigate('Filters', 'sort')}
              style={{
                marginTop: 6,
                marginLeft: -4,
                color: colors.purple_icon
              }}
            />
            <Feather
              name="filter"
              size={24}
              onPress={() => navigation.navigate('Filters', 'filter')}
              style={{
                marginTop: 12,
                marginLeft: 10,
                color: colors.purple_icon
              }}
            />
          </View>
        )
      })
    },
    Profile: {
      screen: Profile,
      navigationOptions: ({ navigation }) => ({
        headerRight: <Favorites navigation={navigation} />
      })
    },
    Chat: {
      screen: Chat,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarVisible: false
      })
    },
    Filters: {
      screen: Filters,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: false
      })
    },
    ReviewForm: {
      screen: ReviewForm,
      navigationOptions: ({ navigation }) => ({
        headerLeft: null,
        tabBarVisible: false
      })
    }
  },
  {
    ...headerProps,
    initialRouteName: 'PsychicList'
  }
)
