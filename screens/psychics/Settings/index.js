import { StackNavigator } from 'react-navigation'
import { headerProps } from '@shared'
import UpdatePsychicProfile from './UpdatePsychicProfile'
import ChangePassword from './../../shared/ChangePassword'

export default StackNavigator(
  {
    UpdatePsychicProfile: {
      screen: UpdatePsychicProfile,
      navigationOptions: ({ navigation }) => ({})
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: ({ navigation }) => ({})
    }
  },
  {
    ...headerProps,
    initialRouteName: 'UpdatePsychicProfile'
  }
)
