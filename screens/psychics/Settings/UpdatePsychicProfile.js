import _ from 'lodash'
import React, { Component } from 'react'
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Dimensions,
  Keyboard
} from 'react-native'
import { Content, Button, Icon, View, ActionSheet, Input } from 'native-base'
import {
  colors,
  easyFirebase,
  MasterStyles,
  notification,
  Sykiks
} from '@services'
import { ImagePicker } from 'expo'
import { Zocial } from '@expo/vector-icons'
import Popover, { PopoverTouchable } from 'react-native-modal-popover'
import { CheckboxGroup, Links, ThumbnailByUid } from '@shared'

var { width } = Dimensions.get('window')

var BUTTONS = ['Available', 'Offline']

@easyFirebase(['profile', '@accountInfo'])
export default class UpdateProfile extends Component {
  state = {}
  profile = {}

  constructor(props) {
    super(props)
    let categories = [
      'Psychic Readings',
      'Tarot',
      'Spiritual counselling',
      'Astrology',
      'Dream Interpretation',
      'Love and Relationships',
      'Numerology'
    ]

    this.state.categories = categories.map((d, i) => ({
      label: d,
      selected: props.profile.categories.includes(d)
    }))
  }

  _pickImage = async () => {
    let { firebase } = this.props
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    })

    if (result.cancelled) return

    result.base64 = 'data:image/jpeg;base64,' + result.base64
    var image = result.base64
    let obj = {}
    obj[this.props.profile.uid] = image
    firebase.ref(`images`).update(obj)
  }

  update() {
    let { firebase } = this.props

    if (!this.props.profile.chatRate) {
      if (this.profile.chatRate == '')
        return notification.showToast('Please Enter Per Minute Fee ')
      if (_.isNumber(this.profile.chatRate))
        return notification.showToast('Please Enter Valid Per Minute Fee')
    }
    if (!this.props.profile.msgRate) {
      if (this.profile.msgRate == '')
        return notification.showToast('Please Enter Per Message Fee ')
      if (_.isNumber(this.profile.msgRate))
        return notification.showToast('Please Enter Valid Per Message Fee')
    }
    if (!this.props.profile.screenName) {
      if (this.profile.screenName == '')
        return notification.showToast('Please Enter Screen Name ')
    }

    if (
      !this.props.accountInfo ||
      (this.props.accountInfo &&
        !this.props.accountInfo.paypalId &&
        !this.profile.paypalId)
    )
      return notification.showToast('Please Enter your Paypal Id ')

    if (this.profile.paypalId) {
      var re = /\S+@\S+\.\S+/
      if (!re.test(this.profile.paypalId))
        return notification.showToast('Please Enter Valid Paypal Id ')
      Sykiks.updatePaypal(this.profile.paypalId)
    }

    let arr = [
      'screenName',
      'chatRate',
      'msgRate',
      'introduction',
      'aboutme',
      'categories'
    ]
    let profile = {}
    arr.forEach(d => {
      if (this.profile[d] && this.profile[d] != this.props.profile[d])
        profile[d] = this.profile[d]
    })
    Keyboard.dismiss()
    firebase.updateProfile(profile)
    notification.showToast('Profile updated')
  }

  setCategories(index) {
    let { categories } = this.state
    var item = categories[index]
    item.selected = !item.selected
    let categoriesLabel = categories
      .filter(d => d.selected)
      .map(d => d.label)
      .join(', ')
    if (categoriesLabel == '') categoriesLabel = 'Specialites'

    this.profile.categories = categoriesLabel
    this.setState({ categories, categoriesLabel })
  }

  render() {
    let { navigate } = this.props.navigation
    let { firebase, accountInfo } = this.props
    let { categoriesLabel } = this.state
    let profile = {
      screenName: '',
      email: firebase.auth().currentUser.email,
      chatRate: '0',
      msgRate: '0',
      introduction: 'Celebrity psychic medium, Top Rated ',
      ...this.props.profile,
      ...accountInfo
    }

    return (
      <KeyboardAvoidingView
        behavior={'padding'}
        style={{ flex: 1 }}
        keyboardVerticalOffset={80}>
        <Content style={MasterStyles.whiteBackground}>
          <View style={styles.headerContent}>
            <View style={styles.logoContainer}>
              <TouchableOpacity onPress={this._pickImage.bind(this)}>
                <ThumbnailByUid uid={profile.uid} style={styles.logo} />
              </TouchableOpacity>
            </View>

            <View style={styles.profileNameView}>
              <Text style={styles.profileName}> {profile.screenName} </Text>
            </View>
          </View>
          <View>
            <Text
              style={{
                backgroundColor: '#F7F7F7',
                padding: 15,
                color: colors.greyText
              }}>
              ACCOUNT INFORMATION
            </Text>
          </View>
          <View style={styles.bar}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.balanceTitle}>Email</Text>
              <Text style={styles.balance}>{profile.email}</Text>
            </View>
          </View>
          <View style={styles.bar}>
            <View style={styles.flexRow}>
              <Text style={styles.statusTitle}>Screen Name</Text>
              <View style={{ width: '70%', flexDirection: 'row' }}>
                <Input
                  defaultValue={profile.screenName}
                  style={[styles.actionListButton]}
                  onChangeText={screenName =>
                    (this.profile.screenName = screenName)
                  }
                />
              </View>
            </View>
          </View>
          <View style={styles.bar}>
            <View style={styles.flexRow}>
              <Text style={styles.titlePer}>Per minute fee</Text>
              <View style={styles.valuePer}>
                <Text>$ </Text>
                <Input
                  defaultValue={profile.chatRate + ''}
                  keyboardType="numeric"
                  style={styles.values}
                  onChangeText={chatRate => (this.profile.chatRate = chatRate)}
                />
              </View>
              <Text style={styles.titlePer}>Per message fee</Text>
              <View style={[styles.valuePer, { borderRightWidth: 0 }]}>
                <Text>$ </Text>
                <Input
                  defaultValue={profile.msgRate + ''}
                  keyboardType="numeric"
                  style={styles.values}
                  onChangeText={msgRate => (this.profile.msgRate = msgRate)}
                />
              </View>
            </View>
          </View>
          <View style={styles.bar}>
            <View style={styles.flexRow}>
              <Text style={styles.statusTitle}>Status</Text>

              <Button
                iconRight
                transparent
                style={styles.actionListButton}
                onPress={() =>
                  ActionSheet.show(
                    {
                      options: BUTTONS,
                      title: 'Status'
                    },
                    buttonIndex => {
                      if (BUTTONS[buttonIndex])
                        firebase.updateProfile({
                          status: BUTTONS[buttonIndex]
                        })
                    }
                  )
                }>
                <Text style={styles.actionListButtonText}>
                  {' '}
                  {profile.status}{' '}
                </Text>
                <Icon name="ios-arrow-down" style={styles.blackIcon} />
              </Button>
            </View>
          </View>
          <View style={styles.bar}>
            <View style={styles.flexRow}>
              <Text style={styles.statusTitle}>Paypal Address</Text>
              <View style={{ width: '70%', flexDirection: 'row' }}>
                <Input
                  defaultValue={profile.paypalId}
                  style={[styles.actionListButton]}
                  onChangeText={paypalId => (this.profile.paypalId = paypalId)}
                />
                <Text style={styles.payPalIcon}>
                  <Zocial name="paypal" style={styles.greyIcon} size={20} />
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.bar}>
            <View>
              <Text style={styles.leftTitle}>Categories</Text>
              <PopoverTouchable>
                <TouchableOpacity>
                  <Text style={styles.categories}>
                    {categoriesLabel || profile.categories}
                  </Text>
                </TouchableOpacity>
                <Popover arrowStyle={{ display: 'none' }}>
                  <CheckboxGroup
                    callback={this.setCategories.bind(this)}
                    checkboxes={this.state.categories}
                  />
                </Popover>
              </PopoverTouchable>
            </View>
          </View>
          <View style={styles.bar}>
            <View>
              <Text style={styles.leftTitle}>Introduction</Text>
              <Input
                multiline
                defaultValue={profile.introduction}
                style={styles.about}
                onChangeText={introduction =>
                  (this.profile.introduction = introduction.trim())
                }
              />
            </View>
          </View>
          <View style={styles.bar}>
            <View>
              <Text style={styles.leftTitle}>About</Text>
              <Input
                multiline
                defaultValue={profile.aboutme}
                style={styles.about}
                onChangeText={aboutme =>
                  (this.profile.aboutme = aboutme.trim())
                }
              />
            </View>
          </View>
          <View style={styles.bar}>
            <TouchableOpacity
              onPress={() => {
                navigate('ChangePassword')
              }}>
              <Text
                style={{
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                  color: colors.purple_icon
                }}>
                Change Password
              </Text>
            </TouchableOpacity>
          </View>
          <Links />
          <View style={styles.flexRow}>
            <Button style={styles.btn} onPress={this.update.bind(this)}>
              <Text style={{ color: colors.white }}>UPDATE</Text>
            </Button>
            <Button style={styles.btn} onPress={() => firebase.logout()}>
              <Text style={{ color: colors.white }}>LOGOUT</Text>
            </Button>
          </View>
        </Content>
      </KeyboardAvoidingView>
    )
  }
}
const styles = StyleSheet.create({
  headerContent: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 15,
    backgroundColor: colors.backgroundGrey,
    paddingTop: '10%'
  },
  profileNameView: {
    margin: 15,
    alignItems: 'center'
  },
  profileName: {
    fontSize: 24,
    color: colors.white,
    fontWeight: 'bold'
  },
  aboutProfile: {
    marginTop: 5,
    color: colors.white
  },
  logoContainer: {
    alignItems: 'stretch',
    flex: 1,
    borderWidth: 10,
    borderColor: colors.white
  },
  logo: {
    width: 200,
    height: 200,
    borderRadius: 0
  },

  btn: {
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    width: '30%',
    marginHorizontal: '10%',
    backgroundColor: colors.purple_icon,
    height: 40
  },
  profileImageContainer: {
    alignItems: 'center',
    marginBottom: '40%'
  },
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.lightestgrey,
    paddingVertical: 10
  },
  balanceTitle: {
    color: colors.GreyTitle,
    width: '30%',
    marginVertical: 7,
    textAlign: 'right',
    fontSize: 12
  },
  balance: {
    marginVertical: 7,
    marginLeft: 10,
    width: '55%',
    textAlign: 'left'
  },
  balanceWithout: {
    marginVertical: 7,
    marginRight: 10,
    textAlign: 'right'
  },
  btnStyle: {
    marginHorizontal: 5,
    marginVertical: 10,
    padding: 7,
    borderRadius: 5,
    height: 35,
    width: '45%',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  buttonText: {
    color: colors.white,
    fontSize: 10
  },
  leftBar: {
    width: '40%',
    flexDirection: 'row'
  },
  rightBar: {
    width: '60%',
    flexDirection: 'row'
  },
  flexRow: {
    flexDirection: 'row'
  },
  titlePer: {
    color: colors.GreyTitle,
    width: '27%',
    paddingVertical: 7,
    textAlign: 'right',
    fontSize: 12
  },
  valuePer: {
    paddingVertical: 7,
    marginLeft: 10,
    width: '20%',
    borderRightWidth: 0.5,
    borderRightColor: colors.lightestgrey,
    flexDirection: 'row'
  },
  values: {
    color: colors.textPurple,
    fontSize: 17,
    height: 30,
    marginRight: 5,
    marginTop: -5,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    backgroundColor: '#FBFBFB'
  },
  statusTitle: {
    color: colors.GreyTitle,
    width: '27%',
    paddingVertical: 7,
    textAlign: 'right',
    fontSize: 12
  },
  actionListButton: {
    marginLeft: 10,
    width: '60%',
    paddingTop: 8,
    paddingLeft: 10,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    backgroundColor: '#FBFBFB'
  },
  actionListButtonText: {
    textAlign: 'left',
    marginTop: -6
  },
  payPalIcon: {
    paddingVertical: 7,
    paddingRight: 10,
    textAlign: 'right',
    marginLeft: 10
    // width:'10%'
  },
  greyIcon: {
    color: colors.greyIcon
  },
  blackIcon: {
    color: colors.black
  },
  headerTitle: {
    // marginVertical: 7,
    // marginLeft: 20,
    // fontWeight: 'bold',
    // fontSize: 16,
    backgroundColor: '#F7F7F7',
    color: colors.greyText,
    padding: 15
  },
  leftTitle: {
    color: colors.GreyTitle,
    marginVertical: 7,
    marginLeft: 15,
    fontSize: 12
  },
  categories: {
    marginVertical: 7,
    marginLeft: 15,
    color: colors.textPurple
  },
  about: {
    marginVertical: 7,
    marginLeft: 15,
    color: colors.grey,
    lineHeight: 22,
    fontSize: 14,
    width: width - 50,
    paddingLeft: 10,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    height: 80,
    backgroundColor: '#FBFBFB',
    borderRadius: 10
  }
})
