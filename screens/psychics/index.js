import React from 'react'
import { TabNavigator } from 'react-navigation'
import { Ionicons } from '@expo/vector-icons'
import { colors } from '@services'
import { MessageCount } from '@shared'
import Profile from './Profile'
import Messages from '../shared/Messages'
import SessionHistory from '../shared/SessionHistory'
import Settings from './Settings'

export default TabNavigator(
  {
    PsychicDashboard: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name={focused ? 'ios-person' : 'ios-person-outline'}
            size={24}
            color="#5A5454"
          />
        )
      }
    },
    Messages: {
      screen: Messages,
      navigationOptions: {
        tabBarIcon: ({ focused }) => <MessageCount focused={focused} />
      }
    },
    SessionHistory: {
      screen: SessionHistory('psychic'),
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name={focused ? 'ios-chatboxes' : 'ios-chatboxes-outline'}
            size={24}
            color="#5A5454"
          />
        )
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <Ionicons
            name={focused ? 'ios-settings' : 'ios-settings-outline'}
            size={24}
            color="#5A5454"
          />
        )
      }
    }
  },
  {
    tabBarPosition: 'bottom',
    initialRouteName: 'PsychicDashboard',
    swipeEnabled: false,
    lazy: true,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      style: {
        backgroundColor: colors.white
      },
      indicatorStyle: {
        backgroundColor: 'white'
      }
    }
  }
)
