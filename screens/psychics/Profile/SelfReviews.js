import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, FlatList } from 'react-native'
import { Content, Button, View } from 'native-base'
import { Ionicons } from '@expo/vector-icons'
import { colors, easyFirebase, Sykiks } from '@services'
import { Review } from '@shared'

@easyFirebase(['reviewsSelf', 'profile'])
export default class ProfileBlock extends Component {
  state = {
    active: 'all'
  }

  render() {
    let { reviewsSelf, profile } = this.props
    let { active } = this.state

    let reviews = Sykiks.toArray(reviewsSelf)

    if (active == 'up') reviews = reviews.filter(d => d.rating == 'up')

    if (active == 'down') reviews = reviews.filter(d => d.rating == 'down')

    return (
      <Content>
        <View>
          <View style={styles.reviewMessageContainer}>
            <TouchableOpacity onPress={() => this.setState({ active: 'all' })}>
              <Text style={active == 'all' ? styles.activeTab : styles.count}>
                Reviews ({profile.rating_up + profile.rating_down})
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ active: 'up' })}>
              <Text style={active == 'up' ? styles.activeTab : styles.count}>
                <Ionicons
                  name="ios-thumbs-up-outline"
                  size={30}
                  color="#7DCD13"
                />
                <Text> {profile.rating_up}</Text>
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ active: 'down' })}>
              <Text style={active == 'down' ? styles.activeTab : styles.count}>
                <Ionicons
                  name="ios-thumbs-down-outline"
                  size={30}
                  color="red"
                />
                <Text> {profile.rating_down}</Text>
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ marginHorizontal: 15 }}>
          <FlatList
            data={reviews}
            keyExtractor={({ key }) => key}
            renderItem={({ item }) => {
              return (
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: colors.greyLine
                  }}>
                  <Review {...item}>
                    <Button style={styles.btn}>
                      <Text style={{ color: colors.white }}> Dispute </Text>
                    </Button>
                  </Review>
                </View>
              )
            }}
          />
        </View>
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  reviewMessageContainer: {
    backgroundColor: colors.purple_icon,
    alignItems: 'center',
    paddingTop: 10,
    flexDirection: 'row'
  },
  activeTab: {
    color: '#4F4F4F',
    alignSelf: 'flex-start',
    backgroundColor: colors.white,
    padding: 15,
    marginLeft: 15,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  count: {
    color: colors.white,
    // alignSelf: 'flex-start',
    padding: 5,
    marginLeft: 5
  },
  btn: {
    marginVertical: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    width: '30%',
    // marginHorizontal:'10%',
    backgroundColor: colors.purple_icon,
    height: 40
  }
})
