import React, { Component } from 'react'
import { StyleSheet, Text } from 'react-native'
import { Content, View, List, ListItem } from 'native-base'
import { colors, easyFirebase, Sykiks } from '@services'
import { DefaultText } from '@shared'
let TransactionCard = history => {
  return (
    <ListItem>
      <View style={styles.bar}>
        <View style={styles.barItem}>
          <Text style={styles.barTop4}>
            + {Sykiks.formatCurrrecy(history.amount)}
          </Text>
        </View>
        <View style={styles.barItem}>
          <Text style={styles.barTopStatus}> {history.status}</Text>
        </View>
        <View style={styles.barItem}>
          <Text style={styles.barTop}>{Sykiks.formatDate(history.date)}</Text>
        </View>
      </View>
    </ListItem>
  )
}

@easyFirebase(['profile', '@payouts'])
export default class TransactionHistory extends Component {
  render() {
    let { payouts } = this.props
    let histArray = payouts

    return (
      <Content
        style={{ backgroundColor: colors.white }}
        contentContainerStyle={{ justifyContent: 'center', marginTop: '25%' }}>
        <View>
          {histArray && (
            <List dataArray={histArray} renderRow={TransactionCard} />
          )}
          {!histArray && <DefaultText text="Sorry! No Payouts to show." />}
        </View>
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row'
  },
  barTop: {
    fontSize: 12
  },
  barTopStatus: {
    fontSize: 12,
    color: colors.greyText,
    alignSelf: 'flex-end'
  },
  barItem: {
    width: '33%',
    padding: 5,
    alignItems: 'center'
  },
  barItem1: {
    width: '25%',
    padding: 5
  },
  barBottom: {
    color: colors.purpleText,
    fontSize: 13
  },
  barTop4: {
    fontSize: 12,
    color: colors.green
  }
})
