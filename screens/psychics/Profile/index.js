import { StackNavigator } from 'react-navigation'
import { headerProps } from '@shared'
import ProfileBlock from './ProfileBlock'
import SelfReviews from './SelfReviews'
import TransactionHistory from './TransactionHistory'

import Chat from './../../shared/Messages/Chat'

export default StackNavigator(
  {
    ProfileBlock: {
      screen: ProfileBlock,
      navigationOptions: ({ navigation }) => ({
        headerLeft: null
      })
    },
    SelfReviews: {
      screen: SelfReviews,
      navigationOptions: ({ navigation }) => ({})
    },
    TransactionHistory: {
      screen: TransactionHistory,
      navigationOptions: ({ navigation }) => ({})
    },
    Chat: {
      screen: Chat,
      navigationOptions: ({ navigation }) => ({
        header: null,
        tabBarVisible: false
      })
    }
  },
  {
    ...headerProps,
    initialRouteName: 'ProfileBlock'
  }
)
