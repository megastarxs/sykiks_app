import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  BackHandler
} from 'react-native'
import { Content, Button, Icon, View, ActionSheet } from 'native-base'
import { MaterialCommunityIcons, Entypo } from '@expo/vector-icons'
import { colors, easyFirebase, Sykiks } from '@services'
import { Review, ImageByUid } from '@shared'
var BUTTONS = ['Available', 'Offline']
@easyFirebase(['reviewsSelf', 'profile', '@accountInfo'])
export default class ProfileBlock extends Component {
  componentDidMount() {
    if (Platform.OS === 'android')
      BackHandler.addEventListener('hardwareBackPress', () => true)
  }

  render() {
    let { navigate } = this.props.navigation
    let account = this.props.accountInfo

    if (!account) account = {}

    let profile = {
      balance: account.balance || 0,
      lastSession: account.lastSession || 0,
      nextPayoutDate: 'NA',
      newMessagesCount: 'NA',
      ...this.props.profile
    }

    let { reviewsSelf, firebase } = this.props
    let reviews = Sykiks.toArray(reviewsSelf)

    return (
      <Content style={{ backgroundColor: colors.white }}>
        <View style={styles.headerContent}>
          <View style={styles.logoContainer}>
            <ImageByUid
              uid={profile.uid}
              style={styles.logo}
              resizeMode="cover"
            />
          </View>

          <View style={styles.profileNameView}>
            <Text style={styles.profileName}>
              {' '}
              {profile.screenName}{' '}
              {/* <MaterialCommunityIcons name='pencil-circle-outline' style={styles.editIcon}></MaterialCommunityIcons> */}
            </Text>
          </View>
        </View>
        <View style={styles.bar}>
          <View style={styles.barItem}>
            <TouchableOpacity
              onPress={() => navigate('TransactionHistory')}
              style={styles.plusIconContainer}>
              <Entypo name="circle-with-plus" style={styles.plusIcon} />
            </TouchableOpacity>
            <Text style={styles.barTop}>
              {Sykiks.formatCurrrecy(profile.balance)}{' '}
            </Text>
            <Text style={styles.barBottom}>Balance</Text>
          </View>
          <View style={styles.barItem}>
            <Text style={styles.barTop}>
              +{Sykiks.formatCurrrecy(profile.lastSession)}
            </Text>
            <Text style={styles.barBottom}>Last Session</Text>
          </View>
          <View style={styles.barItem}>
            <Text style={styles.barTopBlack}>{profile.nextPayoutDate}</Text>
            <Text style={styles.barBottom}>next payout</Text>
          </View>
        </View>
        <View
          style={{
            backgroundColor: colors.white,
            flexDirection: 'row',
            borderBottomWidth: 2,
            borderBottomColor: '#AFAFAF',
            paddingVertical: 15
          }}>
          <View style={{ flexDirection: 'row' }}>
            <Text
              style={{
                color: colors.GreyTitle,
                paddingVertical: 13,
                width: '20%',
                textAlign: 'right',
                fontSize: 12
              }}>
              Status
            </Text>

            <Button
              iconRight
              transparent
              style={styles.actionListButton}
              onPress={() =>
                ActionSheet.show(
                  {
                    options: BUTTONS,
                    title: 'Status'
                  },
                  buttonIndex => {
                    if (BUTTONS[buttonIndex])
                      firebase.updateProfile({
                        status: BUTTONS[buttonIndex]
                      })
                  }
                )
              }>
              <Text style={styles.actionListButtonText}>
                {' '}
                {profile.status}{' '}
              </Text>
              <Icon name="ios-arrow-down" style={styles.blackIcon} />
            </Button>
          </View>
        </View>
        {reviews[0] && (
          <View style={styles.bar}>
            <View
              style={{ flexDirection: 'row', marginTop: 10, marginLeft: 10 }}>
              <Text style={styles.reviewTitle}>Latest Review </Text>
              <TouchableOpacity onPress={() => navigate('SelfReviews')}>
                <MaterialCommunityIcons
                  name="dots-horizontal-circle"
                  style={styles.reviewIcon}
                />
              </TouchableOpacity>
            </View>
            <Review {...reviews[0]} />
          </View>
        )}
      </Content>
    )
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    marginTop: 25
  },
  backArrowIcon: {
    color: '#7E7E7E'
  },
  headerImage: {
    width: 140,
    height: 40
  },
  profileImage: {
    backgroundColor: colors.purple
  },
  profileName: {
    marginTop: 15,
    fontSize: 24,
    color: colors.white,
    fontWeight: 'bold'
  },
  headerContent: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 15,
    backgroundColor: colors.purple_icon
  },
  profileContent: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    width: null,
    padding: 15,
    marginTop: 10,
    backgroundColor: colors.white
  },
  logoContainer: {
    alignItems: 'stretch',
    flex: 1,
    width: 200,
    height: 200,
    borderWidth: 10,
    borderColor: colors.white
  },
  logo: {
    flex: 1
  },
  editIcon: {
    color: colors.white,
    fontSize: 24,
    paddingLeft: 5,
    paddingRight: 5,
    fontWeight: 'bold'
  },
  profileNameView: {
    marginBottom: 15
  },
  bar: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: '#AFAFAF',
    alignItems: 'center'
  },
  barTop: {
    color: colors.green,
    fontSize: 16,
    fontWeight: 'bold'
  },
  barTopBlack: {
    color: colors.black,
    fontSize: 16,
    fontWeight: 'bold'
  },
  barBottom: {
    color: '#AFAFAF',
    fontSize: 14,
    fontWeight: 'bold'
  },
  barItem: {
    width: '33%',
    padding: 18,
    alignItems: 'center'
  },
  plusIcon: {
    color: colors.blueIcon
  },
  plusIconContainer: {
    position: 'absolute',
    top: 15,
    right: 10
  },
  messagePanel: {
    padding: 18,
    alignItems: 'center',
    width: '40%'
  },
  messageText: {
    color: colors.black,
    fontSize: 14
  },
  messageCount: {
    position: 'absolute',
    top: 10,
    fontSize: 38,
    fontWeight: 'bold',
    color: colors.purple_icon
  },
  messagePanelRight: {
    paddingRight: 5,
    width: '60%',
    alignSelf: 'flex-start',
    flexDirection: 'column'
  },
  reviewTitle: {
    color: colors.black,
    fontWeight: 'bold'
  },
  reviewIcon: {
    color: colors.mildGrey,
    fontSize: 24,
    paddingLeft: 5,
    paddingRight: 5,
    fontWeight: 'bold'
  },
  reviewText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.lightTextgrey
  },
  thumbIcon: {
    color: colors.green
  },
  reviewDetails: {
    color: colors.lightTextgrey
  },
  actionListButton: {
    marginLeft: 10,
    width: '75%',
    paddingTop: 8,
    paddingLeft: 10,
    borderWidth: 0.5,
    borderColor: '#ECECEC',
    backgroundColor: '#FBFBFB'
  },
  actionListButtonText: {
    textAlign: 'left',
    marginTop: -6
  },
  blackIcon: {
    color: colors.black
  }
})
