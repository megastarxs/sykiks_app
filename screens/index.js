import React from 'react'

import { AsyncStorage } from 'react-native'
import { StackNavigator } from 'react-navigation'
import { connect } from 'react-redux'

import Tos from './main/Tos'
import Login from './main/Login'

import Registration from './main/Registration'
import Forgotpassword from './main/Forgotpassword'

import Client from './client'
import Psychics from './psychics'
import { navigator, Sykiks } from '@services'
import { Loader, headerProps } from '@shared'
import Alerts from './shared/Alerts'

let noHeader = {
  navigationOptions: ({ navigation }) => ({
    header: null,
    tabBarVisible: false
  })
}

let setNavigationRef = navigatorRef => {
  navigator.setContainer(navigatorRef)
}

let AppNavigator = StackNavigator(
  {
    Login: {
      ...noHeader,
      screen: Login
    },
    Registration: {
      screen: Registration
    },
    Forgotpassword: {
      screen: Forgotpassword
    }
  },
  {
    ...headerProps,
    initialRouteName: 'Login'
  }
)

@connect(({ firebase: { auth, profile } }) => ({ auth, profile }))
export default class Navigator extends React.Component {
  state = {
    acceptedTos: false
  }

  constructor(props) {
    super(props)
    this.postAcceptedTos.bind(this)
  }

  async componentWillMount() {
    try {
      let acceptedTos = (await AsyncStorage.getItem('sykiks.tos')) == 'true'
      this.setState({ acceptedTos })
    } catch (error) {}
  }
  postAcceptedTos() {
    AsyncStorage.setItem('sykiks.tos', 'true')
    this.setState({ acceptedTos: true })
  }
  render() {
    if (!this.state.acceptedTos)
      return <Tos callback={() => this.postAcceptedTos()} />

    let { profile, auth } = this.props

    if (!auth.isLoaded) return <Loader />

    if (auth.isEmpty) return <AppNavigator />

    Sykiks.setProfile(auth.uid, profile)

    if (profile.type)
      return (
        <Alerts>
          {profile.type == 'client' && <Client ref={setNavigationRef} />}
          {profile.type == 'psychic' && <Psychics ref={setNavigationRef} />}
        </Alerts>
      )
    return <Loader />
  }
}
